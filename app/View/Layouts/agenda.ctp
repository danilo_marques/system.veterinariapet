<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="VeterináriaPet">
	<meta name="author" content="VeterináriaPet">
	<link rel="shortcut icon" href="/img/favicon.png">

	<?php echo $this->Html->charset(); ?>
	<title>
		VeterináriaPet - Agenda
	</title>

	<?php 
		echo $this->Html->css(
			array(
				'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800',
				'http://fonts.googleapis.com/css?family=Raleway:300,200,100',
				'bootstrap/bootstrap.css',
				'jquery.gritter/jquery.gritter.css',
				'font-awesome-4/font-awesome.min.css',
				'jquery.nanoscroller/nanoscroller.css',
				'jquery.easypiechart/jquery.easy-pie-chart.css',
				'bootstrap.switch/bootstrap-switch.css',
				'bootstrap.datetimepicker/bootstrap-datetimepicker.min.css',
				'select2/select2.css',
				'bootstrap.slider/slider.css',
				'stylesheets/pygments.css',

				'stylesheets/style.css',
				'stylesheets/style-custom.css',

				'dhtmlx/dhtmlxscheduler.css',
			)
		);
	?>
</head>

<body id="vp-body-agenda">
	<?php echo $this->element('header'); ?>
	<?php echo $this->fetch('content'); ?>

	<?php 
		echo $this->Html->script(
			array(
				'jquery/jquery.js',
				'modernizr/modernizr.js',

				'jquery.nanoscroller/jquery.nanoscroller.js',
				'jquery.sparkline/jquery.sparkline.min.js',
				'jquery.easypiechart/jquery.easy-pie-chart.js',
				'jquery.ui/jquery-ui.js',
				'jquery.nestable/jquery.nestable.js',
				'bootstrap.switch/bootstrap-switch.min.js',
				'bootstrap.datetimepicker/bootstrap-datetimepicker.min.js',
				'select2/select2.min.js',
				'select2/select2_locale_pt-BR.js',
				'bootstrap.slider/bootstrap-slider.js',
				'jquery.gritter/jquery.gritter.min.js',
				'behaviour/general.js',

				'bootstrap/bootstrap.min.js',
				'bootstrap.modal_popover/bootstrap-modal-popover.js',
				'jquery.flot/jquery.flot.js',
				'jquery.flot/jquery.flot.pie.js',
				'jquery.flot/jquery.flot.resize.js',
				'jquery.flot/jquery.flot.labels.js',

				'dhtmlx/dhtmlxscheduler.js',
				'dhtmlx/dhtmlxscheduler_minical.js',
				'dhtmlx/dhtmlxscheduler_tooltip.js',
				'dhtmlx/dhtmlxscheduler_limit.js',
				'dhtmlx/dhtmlxscheduler_brazil_patch.js',
				'dhtmlx/locale_pt.js',
				
				'agenda_configurations.js',
				'format_date.js',
				'strtotime.js',
				'agenda.js',
				'customer.js',

				'common.js',
			)
		);
	?>

	<script type="text/javascript">
		agenda.treatment_duration = "<?php echo $this->Session->read('User.treatment_duration') / 2; ?>";
	</script>
</body>
</html>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="VeterináriaPet">
	<meta name="author" content="VeterináriaPet">
	<link rel="shortcut icon" href="/img/favicon.png">

	<?php echo $this->Html->charset(); ?>
	<title>
		VeterináriaPet - Sistema
	</title>

	<?php 
		echo $this->Html->css(
			array(
				'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800',
				'http://fonts.googleapis.com/css?family=Raleway:300,200,100',
				'bootstrap/bootstrap.css',
				'font-awesome-4/font-awesome.min.css',

				'stylesheets/style.css',
			)
		);
	?>
</head>

<body class="texture">
	<?php echo $this->fetch('content'); ?>

	<?php 
		echo $this->Html->script(
			array(
				'jquery/jquery.js',
				'bootstrap/bootstrap.min.js',
				'modernizr/modernizr.js',
				'jquery.flot/jquery.flot.js',
				'jquery.flot/jquery.flot.pie.js',
				'jquery.flot/jquery.flot.resize.js',
				'jquery.flot/jquery.flot.labels.js',
			)
		);
	?>
</body>
</html>
<div id="cl-wrapper" class="fixed-menu" page="pets" subPage="search">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="block-flat">
                            <div class="header">                          
                                <h3>Busca de pets</h3>
                            </div>
                            <div class="content">
                                <form id="vp-form-search-pet" action="/pets/buscar" method="get" role="form"> 
                                    <div class="form-group">
                                        <label class="vp-label-search-pet">Nome do pet:</label>
                                        <?php echo $this->form->input('name', array('id' => 'vp-form-search-pet-name', 'class' => 'form-control', 'placeholder' => 'Nome do pet', 'label' => false)); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Buscar por:</label>
                                        <div>
                                            <?php echo $this->Form->radio('find_by', array('pet' => 'Nome do pet', 'customer' => 'Nome do cliente'), array('class' => 'vp-form-search-pet-by', 'legend' => false, 'separator' => "&nbsp;&nbsp;&nbsp;", 'default' => 'pet')); ?>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                </form>
                            </div>

                            <!-- Resultado da busca por pets -->
                            <?php if(isset($pets)): ?>
                                <?php if(count($pets) > 0): ?>
                                    <div class="content">
                                        <div class="header">                          
                                            <h4>Resultado da busca</h4>
                                        </div>
                                        <div class="row dash-cols">
                                            <?php foreach($pets as $key => $pet): ?>
                                                <?php
                                                    $customer_name = ''; 
                                                    foreach($pet['Customer'] as $x => $customer) {
                                                        if(count($pet['Customer']) == $x+1) {
                                                            $customer_name .= $customer['name'];
                                                        } else {
                                                            $customer_name .= $customer['name'] . ', ';
                                                        } 
                                                    }
                                                ?>
                                                <div class="col-sm-6 col-md-6 col-lg-4 ">
                                                    <div class="widget-block vp-box-result-search" pet_id="<?php echo $pet['Pet']['id']; ?>">
                                                        <div class="white-box padding">
                                                            <div class="row info">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <img src="http://placehold.it/60x60" alt="Avatar">
                                                                </div>
                                                                <div class="col-sm-8 col-md-8 col-md-offset-1">
                                                                    <div class="vp-search-result-text-overflow">
                                                                        <strong>Pet:</strong> 
                                                                        <?php echo $pet['Pet']['name']; ?>
                                                                    </div>
                                                                    <div class="vp-search-result-text-overflow">
                                                                        <strong>Raça:</strong> 
                                                                        <?php echo $pet['Pet']['race']; ?></div>
                                                                    <div class="vp-search-result-text-overflow">
                                                                        <strong>Cliente:</strong>  
                                                                        <span data-original-title="<?php echo $customer_name; ?>" data-toggle="tooltip" data-placement="top"><?php echo $pet['Customer'][0]['name']; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="content">
                                        <div class="header">                          
                                            <h4>Resultado da busca</h4>
                                        </div>
                                        <div class="alert alert-info">
                                            <i class="fa fa-info-circle sign"></i>Não foi encontradado nenhum <strong>Pet</strong> com o nome digitado. Verifique o nome e tente novamente!
                                         </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>

                            <!-- Resultado da busca por customer -->
                            <?php if(isset($customers)): ?>
                                <?php if(count($customers) > 0): ?>
                                    <div class="content">
                                        <div class="header">                          
                                            <h4>Resultado da busca</h4>
                                        </div>
                                        <div class="row dash-cols">
                                            <?php foreach($customers as $key => $customer): ?>
                                                <?php foreach($customer['Pet'] as $pet): ?>
                                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                                        <div class="widget-block vp-box-result-search" pet_id="<?php echo $pet['id']; ?>">
                                                            <div class="white-box padding">
                                                                <div class="row info">
                                                                    <div class="col-md-3">
                                                                        <img src="http://placehold.it/60x60" alt="Avatar">
                                                                    </div>
                                                                    <div class="col-md-8 col-md-offset-1">
                                                                        <div class="vp-search-result-text-overflow">
                                                                            <strong>Pet:</strong> 
                                                                            <?php echo $pet['name']; ?>
                                                                        </div>
                                                                        <div class="vp-search-result-text-overflow">
                                                                            <strong>Raça:</strong> 
                                                                            <?php echo $pet['race']; ?></div>
                                                                        <div class="vp-search-result-text-overflow">
                                                                            <strong>Cliente:</strong>  
                                                                            <span data-original-title="<?php echo $customer['Customer']['name']; ?>" data-toggle="tooltip" data-placement="top"><?php echo $customer['Customer']['name']; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="content">
                                        <div class="header">                          
                                            <h4>Resultado da busca</h4>
                                        </div>
                                        <div class="alert alert-info">
                                            <i class="fa fa-info-circle sign"></i>Não foi encontradado nenhum <strong>Cliente</strong> com o nome digitado. Verifique o nome e tente novamente!
                                         </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
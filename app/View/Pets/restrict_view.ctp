<div id="cl-wrapper" class="fixed-menu" page="pets">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="block-flat">
                            <div class="header">                          
                                <h3><?php echo $pet['Pet']['name']; ?></h3>
                            </div>
                            <div class="content">
                                <div class="form-group pull-right">
                                    <div class="col-md-4">
                                        <a class="btn btn-sm btn-success" title="Atender" id="vp-btn-popover-treatment" href="/atendimento/<?php echo $pet['Pet']['id']; ?>"><i class="fa fa-check"></i> Ver prontuário</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                            	<form class="form-horizontal">
                                    <h3 class="text-bold text-muted">Dados do pet:</h3>

                            		<div class="form-group">
                                        <label class="col-sm-2 control-label" for="vp-profile-pet-pet-name">Nome do pet</label>
                                        <div class="col-sm-9">
                                            <?php echo $this->form->input('Pet.name', array('value' => $pet['Pet']['name'], 'id' => 'vp-profile-pet-pet-name', 'class' => 'form-control', 'label' => false)) ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="vp-profile-pet-pet-type-id">Tipo do pet</label>
                                        <div class="col-sm-4">
                                            <?php echo $this->form->input('Pet.pet_type_id', array('options' => $pet_types, 'value' => $pet['Pet']['pet_type_id'], 'id' => 'vp-profile-pet-pet-type-id', 'class' => 'select2', 'label' => false)) ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="vp-profile-pet-gender">Sexo do pet</label>
                                        <div class="col-sm-4">
                                            <?php echo $this->form->input('Pet.pet_type_id', array('options' => array('Macho' => 'Macho', 'Fêmea' => 'Fêmea'), 'value' => $pet['Pet']['gender'], 'id' => 'vp-profile-pet-gender', 'class' => 'select2', 'label' => false)) ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="vp-profile-pet-birth">Nascimento</label>
                                        <div class="col-sm-3 col-md-2">
                                            <?php echo $this->form->input('Pet.birth', array('value' => $pet['Pet']['birth'], 'id' => 'vp-profile-pet-birth', 'class' => 'form-control', 'label' => false)) ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="vp-profile-pet-race">Raça</label>
                                        <div class="col-sm-6">
                                            <?php echo $this->form->input('Pet.race', array('value' => $pet['Pet']['race'], 'id' => 'vp-profile-pet-race', 'class' => 'form-control', 'label' => false)) ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="vp-profile-pet-pelage-color">Cor da pelágem</label>
                                        <div class="col-sm-6">
                                            <?php echo $this->form->input('Pet.pelage_color', array('value' => $pet['Pet']['pelage_color'], 'id' => 'vp-profile-pet-pelage-color', 'class' => 'form-control', 'label' => false)) ?>
                                        </div>
                                    </div>

                                    <?php if(count($pet['Customer']) > 1): ?>
                                        <h3 class="text-bold text-muted">Dados dos clientes</h3>
                                    <?php else: ?>
                                        <h3 class="text-bold text-muted">Dados do cliente</h3>
                                    <?php endif; ?>

                                    <?php foreach ($pet['Customer'] as $key => $customer): ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="vp-profile-pet-customer-<?php echo $key; ?>-name">Nome do cliente</label>
                                            <div class="col-sm-9">
                                                <?php echo $this->form->input('Customer.'.$key.'.name', array('value' => $customer['name'], 'id' => 'vp-profile-pet-customer-'.$key.'-name', 'class' => 'form-control', 'label' => false)) ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="vp-profile-pet-customer-<?php echo $key; ?>-birth">Nascimento</label>
                                            <div class="col-sm-3 col-md-2">
                                                <?php echo $this->form->input('Customer.'.$key.'.birth', array('value' => $customer['birth'], 'id' => 'vp-profile-pet-customer-'.$key.'-birth', 'class' => 'form-control', 'label' => false)) ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="vp-profile-pet-customer-<?php echo $key; ?>-gender">Sexo</label>
                                            <div class="col-sm-4">
                                                <?php echo $this->form->input('Customer.'.$key.'.gender', array('options' => array('Masculino' => 'Masculino', 'Feminino' => 'Feminino'), 'value' => $customer['gender'], 'id' => 'vp-profile-pet-customer-'.$key.'-gender', 'class' => 'select2', 'label' => false)) ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="vp-profile-pet-customer-<?php echo $key; ?>-phone">Telefone</label>
                                            <div class="col-sm-3 col-md-3">
                                                <?php echo $this->form->input('Customer.'.$key.'.phone', array('value' => $customer['phone'], 'id' => 'vp-profile-pet-customer-'.$key.'-phone', 'class' => 'form-control', 'label' => false)) ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="vp-profile-pet-customer-<?php echo $key; ?>-cellphone">Celular</label>
                                            <div class="col-sm-3 col-md-3">
                                                <?php echo $this->form->input('Customer.'.$key.'.cellphone', array('value' => $customer['cellphone'], 'id' => 'vp-profile-pet-customer-'.$key.'-cellphone', 'class' => 'form-control', 'label' => false)) ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="vp-profile-pet-customer-<?php echo $key; ?>-commercial-phone">Tel. Comercial</label>
                                            <div class="col-sm-3 col-md-3">
                                                <?php echo $this->form->input('Customer.'.$key.'.commercial_phone', array('value' => $customer['commercial_phone'], 'id' => 'vp-profile-pet-customer-'.$key.'-commercial-phone', 'class' => 'form-control', 'label' => false)) ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="vp-profile-pet-customer-<?php echo $key; ?>-email">E-mail</label>
                                            <div class="col-sm-6 col-md-6">
                                                <?php echo $this->form->input('Customer.'.$key.'.email', array('value' => $customer['email'], 'id' => 'vp-profile-pet-customer-'.$key.'-email', 'class' => 'form-control', 'label' => false)) ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>

                                    <div class="form-group">
				                        <div class="col-md-4">
				                            <input type="submit" class="btn btn-primary" value="Salvar">
				                        </div>
				                    </div>
                            	</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
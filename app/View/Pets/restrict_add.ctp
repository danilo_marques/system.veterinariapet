<div id="cl-wrapper" class="fixed-menu" page="pets" subPage="add">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <section class="panel form-wizard" id="vp-form-wizard-add-pet">
                        <header class="panel-heading">
                            <h2 class="panel-title"><strong>Cadastro passo a passo</strong></h2>
                        </header>
                        <div class="panel-body">
                            <div class="wizard-progress wizard-progress-lg">
                                <div class="steps-progress">
                                    <div class="progress-indicator"></div>
                                </div>
                                <ul class="wizard-steps">
                                    <li class="active">
                                        <a href="#vp-data-customer" data-toggle="tab"><span>1</span>Cadastro do cliente</a>
                                    </li>
                                    <li>
                                        <a href="#vp-contact-info" data-toggle="tab"><span>2</span>Informações de contato</a>
                                    </li>
                                    <li>
                                        <a href="#vp-data-pet" data-toggle="tab"><span>3</span>Cadastro do pet</a>
                                    </li>
                                    <li>
                                        <a href="#vp-confirm" data-toggle="tab"><span>4</span>Confirmar dados</a>
                                    </li>
                                </ul>
                            </div>

                            <form action="/pets/adicionar" method="post" class="form-horizontal" novalidate="novalidate" id="vp-form-add-pet">
                                <div class="tab-content">
                                    <div id="vp-data-customer" class="tab-pane active">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-name">Nome completo</label>
                                            <div class="col-sm-9">
                                                <?php echo $this->form->input('Customer.name', array('id' => 'vp-form-add-pet-customer-name', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-birth">Nascimento</label>
                                            <div class="col-sm-3 col-md-3">
                                                <?php echo $this->form->input('Customer.birth', array('id' => 'vp-form-add-pet-customer-birth', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-gender">Sexo</label>
                                            <div class="col-sm-3">
                                                <?php echo $this->form->input('Customer.gender', array('options' => array('Masculino' => 'Masculino', 'Feminino' => 'Feminino'), 'empty' => 'Selecionar', 'id' => 'vp-form-add-pet-customer-gender', 'class' => 'select2', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-email">Email</label>
                                            <div class="col-sm-9">
                                                <?php echo $this->form->input('Customer.email', array('id' => 'vp-form-add-pet-customer-email', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-password">Senha</label>
                                            <div class="col-sm-6">
                                                <?php echo $this->form->password('Customer.password', array('id' => 'vp-form-add-pet-customer-password', 'class' => 'form-control', 'label' => false, 'div' => false, /* required, 'minlength' => '3' */)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="vp-contact-info" class="tab-pane">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-phone">Telefone</label>
                                            <div class="col-sm-4">
                                                <?php echo $this->form->input('Customer.phone', array('id' => 'vp-form-add-pet-customer-phone', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-cellphone">Celular</label>
                                            <div class="col-sm-4">
                                                <?php echo $this->form->input('Customer.cellphone', array('id' => 'vp-form-add-pet-customer-cellphone', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-customer-commercial_phone">Telefone comercial</label>
                                            <div class="col-sm-4">
                                                <?php echo $this->form->input('Customer.commercial_phone', array('id' => 'vp-form-add-pet-customer-commercial_phone', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-address-zip">CEP</label>
                                            <div class="col-sm-3">
                                                <?php echo $this->form->input('Address.zip', array('id' => 'vp-form-add-pet-address-zip', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-address-address">Logradouro</label>
                                            <div class="col-sm-9">
                                                <?php echo $this->form->input('Address.address', array('id' => 'vp-form-add-pet-address-address', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-address-number">Número</label>
                                            <div class="col-sm-3">
                                                <?php echo $this->form->input('Address.number', array('id' => 'vp-form-add-pet-address-number', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-address-complement">Complemento</label>
                                            <div class="col-sm-6">
                                                <?php echo $this->form->input('Address.complement', array('id' => 'vp-form-add-pet-address-complement', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-address-state">Estado</label>
                                            <div class="col-sm-3">
                                                <?php echo $this->form->input('Address.state', array('id' => 'vp-form-add-pet-address-state', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-address-city">Cidade</label>
                                            <div class="col-sm-6">
                                                <?php echo $this->form->input('Address.city', array('id' => 'vp-form-add-pet-address-city', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-address-district">Bairro</label>
                                            <div class="col-sm-9">
                                                <?php echo $this->form->input('Address.district', array('id' => 'vp-form-add-pet-address-district', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="vp-data-pet" class="tab-pane">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-pet-name">Nome do pet</label>
                                            <div class="col-sm-9">
                                                <?php echo $this->form->input('Pet.name', array('id' => 'vp-form-add-pet-pet-name', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-pet-pet_type_id">Tipo do pet</label>
                                            <div class="col-sm-4">
                                                <?php echo $this->form->input('Pet.pet_type_id', array('options' => $pet_type, 'empty' => 'Selecionar', 'id' => 'vp-form-add-pet-pet-pet_type_id', 'class' => 'select2', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-pet-gender">Sexo do pet</label>
                                            <div class="col-sm-4">
                                                <?php echo $this->form->input('Pet.gender', array('options' => array('Macho' => 'Macho', 'Femêa' => 'Femêa'), 'empty' => 'Selecionar', 'id' => 'vp-form-add-pet-pet-gender', 'class' => 'select2', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-pet-birth">Nascimento</label>
                                            <div class="col-sm-3">
                                                <?php echo $this->form->input('Pet.birth', array('id' => 'vp-form-add-pet-pet-birth', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-pet-race">Raça</label>
                                            <div class="col-sm-6">
                                                <?php echo $this->form->input('Pet.race', array('id' => 'vp-form-add-pet-pet-race', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-form-add-pet-pet-pelage_color">Cor da pelágem</label>
                                            <div class="col-sm-6">
                                                <?php echo $this->form->input('Pet.pelage_color', array('id' => 'vp-form-add-pet-pet-pelage_color', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="vp-confirm" class="tab-pane">
                                        <hr class="dotted short">
                                        <h5 class="text-muted">Dados do cliente</h5>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-customer-name">Nome completo</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-customer-name" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-customer-birth">Nascimento</label>
                                            <div class="col-sm-3 col-md-3">
                                                <input type="text" class="form-control" id="vp-confirm-customer-birth" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-customer-gender">Sexo</label>
                                            <div class="col-sm-3">
                                                <select id="vp-confirm-customer-gender" class="select2" disabled>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Feminino">Feminino</option>
                                                <select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-customer-email">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-customer-email" disabled>
                                            </div>
                                        </div>

                                        <hr class="dotted short">
                                        <h5 class="text-muted">Infomações de contato</h5>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-customer-phone">Telefone</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="vp-confirm-customer-phone" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-customer-cellphone">Celular</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="vp-confirm-customer-cellphone" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-customer-commercial_phone">Telefone comercial</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="vp-confirm-customer-commercial_phone" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-address-zip">CEP</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vp-confirm-address-zip" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-address-address">Logradouro</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-address-address" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-address-number">Número</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vp-confirm-address-number" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-address-complement">Complemento</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-address-complement" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-address-state">Estado</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vp-confirm-address-state" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-address-city">Cidade</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-address-city" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-address-district">Bairro</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-address-district" disabled>
                                            </div>
                                        </div>

                                        <hr class="dotted short">
                                        <h5 class="text-muted">Dados do pet</h5>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-pet-name">Nome do pet</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-pet-name" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-pet-pet_type">Tipo do pet</label>
                                            <div class="col-sm-4">
                                                <select class="select2" id="vp-confirm-pet-pet_type" disabled>
                                                    <option value="1">Cachorro</option>
                                                    <option value="2">Gato</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-pet-gender">Sexo do pet</label>
                                            <div class="col-sm-4">
                                                <select class="select2" id="vp-confirm-pet-gender" disabled>
                                                    <option value="Macho">Macho</option>
                                                    <option value="Fêmea">Fêmea</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-pet-birth">Nascimento</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vp-confirm-pet-birth" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-pet-race">Raça</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-pet-race" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="vp-confirm-pet-pelage_color">Cor da pelágem</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="vp-confirm-pet-pelage_color" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <ul class="pager">
                                <li class="previous disabled">
                                    <a><i class="fa fa-angle-left"></i> Anterior</a>
                                </li>
                                <li class="finish hidden pull-right">
                                    <a>Cadastrar</a>
                                </li>
                                <li class="next">
                                    <a>Próximo <i class="fa fa-angle-right"></i></a>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('AgendaConfigurations/restrict_confirm_delete'); ?>
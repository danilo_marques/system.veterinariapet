<div id="cl-wrapper" class="fixed-menu" page="configurations" subPage="times">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="page-head">
            <h2>Configuração de horários</h2>
            <ol class="breadcrumb">
              <li><a href="/agenda">Agenda</a></li>
              <li>Configurações</li>
              <li class="active">Horários da agenda</li>
            </ol>
        </div>
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <form class="form-horizontal" id="vp-form-agenda-configuration" method="post" action="/configuracoes/agenda">

                        <?php echo $this->form->hidden('AgendaConfiguration.office_unit_id', array('value' => $this->Session->read('User.office_unit_id'))); ?>
                        <?php echo $this->form->hidden('AgendaConfiguration.user_id', array('value' => $this->Session->read('Auth.User.id'))); ?>
                        <?php echo $this->form->hidden('OfficeUnitUser.id', array('value' => $this->Session->read('User.office_unit_user_id'))); ?>
                        <?php echo $this->form->hidden('User.id', array('value' => $this->Session->read('User.office_unit_user_id'))); ?>

                        <div class="col-md-12 col-lg-4 col-xl-4">
                            <section class="panel panel-featured-left panel-featured-primary">
                                <div class="panel-body">
                                    <h5 class="text-bold text-muted">Duração média das consultas:</h5>
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <?php echo $this->form->text('OfficeUnitUser.treatment_duration', array('class' => 'form-control', 'placeholder' => '30', 'div' => false, 'label' => false)); ?>
                                            <i class="text-success" style="font-size:11px;"><strong>Em minutos</strong></i>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-12 col-lg-8 col-xl-8">
                            <section class="panel panel-featured-left panel-featured-primary">
                                <div class="panel-body">
                                   <h5 class="text-bold text-muted">Dias de funcionamento:</h5>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="checkbox-inline">
                                                <div class="checkbox-custom checkbox-default">
                                                    <?php echo $this->form->checkbox('Agenda.0.weekday', array('id' => 'vp-form-agenda-configuration-zero', 'hiddenField' => false, 'value' => 0)); ?>
                                                    <label for="vp-form-agenda-configuration-zero">Domingo</label>
                                                </div>
                                            </label>
                                            <label class="checkbox-inline">
                                                <div class="checkbox-custom checkbox-default">
                                                    <?php echo $this->form->checkbox('Agenda.1.weekday', array('id' => 'vp-form-agenda-configuration-one', 'hiddenField' => false, 'value' => 1, 'checked' => 'checked')); ?>
                                                    <label for="vp-form-agenda-configuration-one">Segunda</label>
                                                </div>
                                            </label>
                                            <label class="checkbox-inline">
                                                <div class="checkbox-custom checkbox-default">
                                                    <?php echo $this->form->checkbox('Agenda.2.weekday', array('id' => 'vp-form-agenda-configuration-two', 'hiddenField' => false, 'value' => 2, 'checked' => 'checked')); ?>
                                                    <label for="vp-form-agenda-configuration-two">Terça</label>
                                                </div>
                                            </label>
                                            <label class="checkbox-inline">
                                                <div class="checkbox-custom checkbox-default">
                                                    <?php echo $this->form->checkbox('Agenda.3.weekday', array('id' => 'vp-form-agenda-configuration-three', 'hiddenField' => false, 'value' => 3, 'checked' => 'checked')); ?>
                                                    <label for="vp-form-agenda-configuration-three">Quarta</label>
                                                </div>
                                            </label>
                                            <label class="checkbox-inline">
                                                <div class="checkbox-custom checkbox-default">
                                                    <?php echo $this->form->checkbox('Agenda.4.weekday', array('id' => 'vp-form-agenda-configuration-four', 'hiddenField' => false, 'value' => 4, 'checked' => 'checked')); ?>
                                                    <label for="vp-form-agenda-configuration-four">Quinta</label>
                                                </div>
                                            </label>
                                            <label class="checkbox-inline">
                                                <div class="checkbox-custom checkbox-default">
                                                    <?php echo $this->form->checkbox('Agenda.5.weekday', array('id' => 'vp-form-agenda-configuration-five', 'hiddenField' => false, 'value' => 5, 'checked' => 'checked')); ?>
                                                    <label for="vp-form-agenda-configuration-five">Sexta</label>
                                                </div>
                                            </label>
                                            <label class="checkbox-inline">
                                                <div class="checkbox-custom checkbox-default">
                                                    <?php echo $this->form->checkbox('Agenda.6.weekday', array('id' => 'vp-form-agenda-configuration-six', 'hiddenField' => false, 'value' => 6, 'checked' => 'checked')); ?>
                                                    <label for="vp-form-agenda-configuration-six">Sábado</label>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                         <div class="clearfix"></div>

                        <div class="col-md-12 col-lg-4 col-xl-4">
                            <section class="panel panel-featured-left panel-featured-primary">
                                <div class="panel-body">
                                    <h5 class="text-bold text-muted">Horário de funcionamento:</h5>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label" for="vp-form-agenda-configuration-start-time">Das:</label>
                                        <div class="col-sm-4">
                                            <?php echo $this->form->text('AgendaConfiguration.start_time', array('id' => 'vp-form-agenda-configuration-start-time', 'placeholder' => '09:00', 'class' => 'form-control')); ?>
                                        </div>
                                        <label class="col-sm-1 control-label" for="vp-form-agenda-configuration-end-time">às:</label>
                                        <div class="col-sm-4">
                                            <?php echo $this->form->text('AgendaConfiguration.end_time', array('id' => 'vp-form-agenda-configuration-end-time', 'placeholder' => '18:00', 'class' => 'form-control')); ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-12 col-lg-4 col-xl-4">
                            <section class="panel panel-featured-left panel-featured-primary">
                                <div class="panel-body">
                                    <h5 class="text-bold text-muted">Horário da pausa:</h5>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label" for="vp-form-agenda-configuration-start-time-lunch">Das:</label>
                                        <div class="col-sm-4">
                                            <?php echo $this->form->text('AgendaConfiguration.start_time_lunch', array('id' => 'vp-form-agenda-configuration-start-time-lunch', 'placeholder' => '12:00', 'class' => 'form-control')); ?>
                                        </div>
                                        <label class="col-sm-1 control-label" for="vp-form-agenda-configuration-end-time-lunch">às:</label>
                                        <div class="col-sm-4">
                                            <?php echo $this->form->text('AgendaConfiguration.end_time_lunch', array('id' => 'vp-form-agenda-configuration-end-time-lunch', 'placeholder' => '13:00', 'class' => 'form-control')); ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-12 col-lg-4 col-xl-4">
                            <section class="panel panel-featured-left panel-featured-primary">
                                <div class="panel-body">
                                    <h5 class="text-bold text-muted">Ações da agenda</h5>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <input type="submit" value="Salvar" class="btn btn-info">
                                        </div>
                                        <?php if(!empty($configurations)) : ?>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0);" class="btn btn-danger vp-delete-all-period">Apagar tudo</a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </form>

                    <div class="clearfix"></div>

                    <?php if(!empty($configurations)) : ?>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <h2 class="panel-title"><i class="fa fa-cog"></i> Horários Configurados</h2>
                            </header>
                            <div class="panel-body">
                                <table class="table table-no-more table-bordered table-striped mb-none">
                                    <thead>
                                        <tr>
                                            <th>Dias de funcionamento</th>
                                            <th class="hidden-xs hidden-sm">Horário de funcionamento</th>
                                            <th class="text-left">Horário da pausa</th>
                                            <th class="text-left hidden-xs hidden-sm">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($configurations as $key => $configuration) : ?>
                                            <tr>
                                                <td data-title="Funcionamento"><?php echo $this->Agenda->weekday($configuration['AgendaConfiguration']['weekday']); ?></td>
                                                <td data-title="Horário"><?php echo $configuration['AgendaConfiguration']['start_time']; ?> às <?php echo $configuration['AgendaConfiguration']['end_time']; ?></td>
                                                <td data-title="Pausa"><?php echo $configuration['AgendaConfiguration']['start_time_lunch']; ?> às <?php echo $configuration['AgendaConfiguration']['end_time_lunch']; ?></td>
                                                <td data-title="Ações" class="text-center">
                                                    <a href="javascript:void(0);" agenda_configuration_id="<?php echo $configuration['AgendaConfiguration']['id']; ?>" class="vp-delete-period btn-sm btn-danger">Apagar</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('AgendaConfigurations/restrict_confirm_delete'); ?>
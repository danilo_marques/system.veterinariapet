<div class="container">
    <div class="row">
        <div class="col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
            <div class="block-flat">
                <div class="header">
                    <h3>Configuração básica VeterináriaPet</h3>
                </div>
                <div class="content">
                    <form id="vp-form-config-first-access" action="/configuracao/agenda" method="post" class="form-horizontal">

                        <?php echo $this->form->hidden('AgendaConfiguration.office_unit_id', array('value' => $this->Session->read('User.office_unit_id'))); ?>
                        <?php echo $this->form->hidden('AgendaConfiguration.user_id', array('value' => $this->Session->read('Auth.User.id'))); ?>
                        <?php echo $this->form->hidden('OfficeUnitUser.id', array('value' => $this->Session->read('User.office_unit_user_id'))); ?>
                        <?php echo $this->form->hidden('User.id', array('value' => $this->Session->read('User.office_unit_user_id'))); ?>
                        <?php echo $this->form->hidden('User.first_access', array('value' => 0)); ?>
                        <?php echo $this->form->hidden('first_access', array('value' => true)); ?>

                        <h5>Duração média das consultas:</h5>
                        <div class="form-group">
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                                 <?php echo $this->form->text('OfficeUnitUser.treatment_duration', array('class' => 'form-control', 'id' => 'vp-form-first-access-treatment-duration', 'placeholder' => '30', 'div' => false, 'label' => false)); ?>
                                <i class="text-info"><strong>Em minutos</strong></i>
                            </div>
                        </div>

                        <h5>Horário de funcionamento:</h5>
                        <div class="form-group">
                            <label class="col-sm-1 col-md-1 control-label" for="vp-form-first-access-start-time">Das:</label>
                            <div class="col-sm-4 col-md-3 col-lg-2">
                                <?php echo $this->form->text('AgendaConfiguration.start_time', array('id' => 'vp-form-first-access-start-time', 'placeholder' => '09:00', 'class' => 'form-control')); ?>
                            </div>
                            <label class="col-sm-1 col-md-1 control-label" for="vp-form-first-access-end-time">às:</label>
                            <div class="col-sm-4 col-md-3 col-lg-2">
                                <?php echo $this->form->text('AgendaConfiguration.end_time', array('id' => 'vp-form-first-access-end-time', 'placeholder' => '18:00', 'class' => 'form-control')); ?>
                            </div>
                        </div>

                        <h5>Horário da pausa:</h5>
                        <div class="form-group">
                            <label class="col-sm-1 col-md-1 control-label" for="vp-form-first-access-start-time-lunch">Das:</label>
                            <div class="col-sm-4 col-md-3 col-lg-2">
                                <?php echo $this->form->text('AgendaConfiguration.start_time_lunch', array('id' => 'vp-form-first-access-start-time-lunch', 'placeholder' => '12:00', 'class' => 'form-control')); ?>
                            </div>
                            <label class="col-sm-1 col-md-1 control-label" for="vp-form-first-access-end-time-lunch">às:</label>
                            <div class="col-sm-4 col-md-3 col-lg-2">
                                <?php echo $this->form->text('AgendaConfiguration.end_time_lunch', array('id' => 'vp-form-first-access-end-time-lunch', 'placeholder' => '13:00', 'class' => 'form-control')); ?>
                            </div>
                        </div>

                        <h5>Dias de funcionamento:</h5>
                        <div class="form-group">
                            <div class="col-md-10">
                                <label class="checkbox-inline">
                                    <div class="checkbox-custom checkbox-default">
                                        <?php echo $this->form->checkbox('Agenda.0.weekday', array('id' => 'vp-form-first-access-weekday-zero', 'hiddenField' => false, 'value' => 0)); ?>
                                        <label for="vp-form-first-access-weekday-zero">Domingo</label>
                                    </div>
                                </label>
                                <label class="checkbox-inline">
                                    <div class="checkbox-custom checkbox-default">
                                        <?php echo $this->form->checkbox('Agenda.1.weekday', array('id' => 'vp-form-first-access-weekday-one', 'hiddenField' => false, 'value' => 1, 'checked' => 'checked')); ?>
                                        <label for="vp-form-first-access-weekday-one">Segunda</label>
                                    </div>
                                </label>
                                <label class="checkbox-inline">
                                    <div class="checkbox-custom checkbox-default">
                                        <?php echo $this->form->checkbox('Agenda.2.weekday', array('id' => 'vp-form-first-access-weekday-two', 'hiddenField' => false, 'value' => 2, 'checked' => 'checked')); ?>
                                        <label for="vp-form-first-access-weekday-two">Terça</label>
                                    </div>
                                </label>
                                <label class="checkbox-inline">
                                    <div class="checkbox-custom checkbox-default">
                                        <?php echo $this->form->checkbox('Agenda.3.weekday', array('id' => 'vp-form-first-access-weekday-three', 'hiddenField' => false, 'value' => 3, 'checked' => 'checked')); ?>
                                        <label for="vp-form-first-access-weekday-three">Quarta</label>
                                    </div>
                                </label>
                                <label class="checkbox-inline">
                                    <div class="checkbox-custom checkbox-default">
                                        <?php echo $this->form->checkbox('Agenda.4.weekday', array('id' => 'vp-form-first-access-weekday-four', 'hiddenField' => false, 'value' => 4, 'checked' => 'checked')); ?>
                                        <label for="vp-form-first-access-weekday-four">Quinta</label>
                                    </div>
                                </label>
                                <label class="checkbox-inline">
                                    <div class="checkbox-custom checkbox-default">
                                        <?php echo $this->form->checkbox('Agenda.5.weekday', array('id' => 'vp-form-first-access-weekday-five', 'hiddenField' => false, 'value' => 5, 'checked' => 'checked')); ?>
                                        <label for="vp-form-first-access-weekday-five">Sexta</label>
                                    </div>
                                </label>
                                <label class="checkbox-inline">
                                    <div class="checkbox-custom checkbox-default">
                                        <?php echo $this->form->checkbox('Agenda.6.weekday', array('id' => 'vp-form-first-access-weekday-six', 'hiddenField' => false, 'value' => 6, 'checked' => 'checked')); ?>
                                        <label for="vp-form-first-access-weekday-six">Sábado</label>
                                    </div>
                                </label>
                            </div>
                        </div>

                        <h5>Permitir agendamento online:</h5>
                        <div class="form-group">
                            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                <div class="radio-custom">
                                    <input type="radio" name="data[OfficeUnitUser][online]" value="1" id="vp-form-first-access-online-true" checked="checked">
                                    <label for="vp-form-first-access-online-true">Sim</label>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                <div class="radio-custom">
                                    <input type="radio" name="data[OfficeUnitUser][online]" value="0" id="vp-form-first-access-online-false">
                                    <label for="vp-form-first-access-online-false">Não</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-5 pull-right info-first-config">
                            <h5 class="text-info text-bold">Porque preciso fazer essas configurações?</h5>
                            <p>Essas primeiras informções serverm para configurar a sua agenda de atendimentos tanto online quanto em sua clínica veterinária.</p>
                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Salvar</button>

                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
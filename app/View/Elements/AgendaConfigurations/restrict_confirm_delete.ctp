<div class="modal fade" id="vp-lightbox-confirm-delete" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<?php echo $this->form->hidden('AgendaConfiguration.id', array('id' => 'agenda-configuration-id')); ?>
				<?php echo $this->form->hidden('Action', array('id' => 'action')); ?>
				<div class="text-center">
					<div class="i-circle danger"><i class="fa fa-question"></i></div>
					<h4>Exclusão de configuração</h4>
					<p id="vp-text-confirmation"></p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger" id="vp-btn-delete-config-agenda" data-dismiss="modal">Apagar</button>
			</div>
		</div>
	</div>
</div>
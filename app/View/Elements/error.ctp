<div class="alert alert-danger alert-white rounded">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<div class="icon"><i class="fa fa-times-circle"></i></div>
	<strong>Erro!</strong> <?php echo $message; ?>
</div>
<div class="cl-sidebar">
    <div class="cl-toggle"><i class="fa fa-bars"></i></div>
    <div class="cl-navblock">
        <div class="menu-space">
            <div class="content">
                <div class="side-user">
                    <div class="avatar"><img src="/img/template/avatar1_50.jpg" alt="Avatar" /></div>
                    <div class="info">
                        <a href="#"><?php echo $this->Session->read('Auth.User.name'); ?></a>
                        <span>
                            <strong><?php echo $this->Session->read('Auth.User.type_enum'); ?></strong>
                        </span>
                    </div>
                </div>
                <ul class="cl-vnavigation">
                    <li class="menu-agenda"><a href="/agenda"><i class="fa fa-calendar"></i><span>Agenda</span></a></li>

                    <li class="menu-dashboard"><a href="/dashboard"><i class="fa fa-bar-chart"></i><span>Painel</span></a></li>

                    <li class="menu-pets"><a href="#"><i class="fa fa-paw"></i><span>Pets</span></a>
                        <ul class="sub-menu">
                            <li class="menu-pets-add"><a href="/pets/adicionar">Cadastrar</a></li>
                            <li class="menu-pets-search"><a href="/pets/buscar">Buscar</a></li>
                        </ul>
                    </li>

                    <li class="menu-money"><a href="#"><i class="fa fa-money"></i><span>Financeiro</span></a>
                        <ul class="sub-menu">
                            <li><a href="#">Contas a pagar</a></li>
                        </ul>
                    </li>

                    <li class="menu-configurations"><a href="#"><i class="fa fa-cogs"></i><span>Configurações</span></a>
                        <ul class="sub-menu">
                            <li class="menu-configurations-times"><a href="/configuracoes/agenda">Horários da agenda</a></li>
                            <li class="menu-configurations-treatments"><a href="/configuracoes/prontuarios">Prontuários</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="text-right collapse-button" style="padding:7px 9px;">
            <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
        </div>
    </div>
</div>
<div class="alert alert-success alert-white rounded">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<div class="icon"><i class="fa fa-check"></i></div>
	<strong>Sucesso!</strong> <?php echo $message; ?>
</div>
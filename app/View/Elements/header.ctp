<!-- Fixed navbar -->
<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="fa fa-gear"></span>
            </button>
            <a class="navbar-brand" href="/agenda"><span>VeterináriaPet</span></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="menu-agenda"><a href="/agenda">Agenda</a></li>
                <li class="menu-dashboard"><a href="/dashboard">Painel</a></li>
                <li class="menu-configurations" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configurações <b class="caret"></b></a>
                    <ul class="dropdown-menu col-menu-2">
                        <li class="no-padding">
                            <ul>
                                <li class="dropdown-header"><i class="fa fa-gear"></i>Configurações</li>
                                <li class="menu-configurations-times"><a href="/configuracoes/agenda">Horários da agenda</a></li>
                                <li class="menu-configurations-treatments"><a href="/configuracoes/prontuarios">Prontuários</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right user-nav">
                <li class="dropdown profile_menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img alt="Avatar" src="/img/template/avatar2.jpg" />
                        <?php echo $this->Session->read('Auth.User.name'); ?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i>&nbsp; Meu perfil</a></li>
                        <li><a href="#"><i class="fa fa-question-circle"></i>&nbsp; Ajuda</a></li>
                        <li class="divider"></li>
                        <li><a href="/logout"><i class="fa fa-power-off"></i>&nbsp; Sair</a></li>
                    </ul>
                </li>
            </ul>           
            <ul class="nav navbar-nav navbar-right not-nav">
                <li class="button dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-comments"></i><span class="bubble">2</span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="nano nscroller">
                                <div class="content">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-cloud-upload info"></i><b>Suporte:</b> Abertura de chamado <span class="date">2 minutos atrás.</span></a></li>
                                        <li><a href="#"><i class="fa fa-credit-card danger"></i> <b>Sistema:</b> Nova funcionalidade <span class="date">1 hora atrás.</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="foot"><li><a href="javascript:void(0);">Ver todos</a></li></ul>           
                        </li>
                    </ul>
                </li>               
            </ul>
        </div>
    </div>
</div>
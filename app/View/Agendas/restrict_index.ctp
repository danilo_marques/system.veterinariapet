<div id="cl-wrapper" class="fixed-menu" page="agenda">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div class="col-sm-12 col-md-12 col-lg-12 text-right">
                <button type="button" id="vp-btn-new-scheduling" class="btn btn-sm btn-success"><i class="fa fa-calendar"></i> Agendar Consulta</button>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="dhtmlx_scheduler">
                    <div id="init_scheduler" class="dhx_cal_container" style='width:100%; height:100%;'>
                        <div class="dhx_cal_navline">
                            <div class="dhx_cal_prev_button">&nbsp;</div>
                            <div class="dhx_cal_next_button">&nbsp;</div>
                            <div class="dhx_cal_today_button"></div>
                            <div class="dhx_cal_date"></div>
                            <div class="dhx_minical_icon" id="dhx_minical_icon" onclick="agenda.show_minical()">&nbsp;</div>
                            <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
                            <div class="dhx_cal_tab dhx_cal_tab_half" name="week_tab" style="right:140px;"></div>
                            <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
                        </div>
                        <div class="dhx_cal_header"></div>
                        <div class="dhx_cal_data"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('../Agendas/restrict_scheduling_lightbox'); ?>
<?php echo $this->element('../Agendas/restrict_rescheduling_lightbox'); ?>
<?php echo $this->element('../Agendas/restrict_scheduling_successful'); ?>
<?php echo $this->element('../Agendas/restrict_rescheduling_successful'); ?>
<?php echo $this->element('../Agendas/restrict_delete_scheduling'); ?>
<?php echo $this->element('../Agendas/restrict_popover'); ?>
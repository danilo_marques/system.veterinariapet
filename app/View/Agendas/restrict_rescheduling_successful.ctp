<div class="modal fade" id="vp-lightbox-rescheduling-successful" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="i-circle success"><i class="fa fa-check"></i></div>
					<h4>Consulta reagendada com sucesso!</h4>
					<p>Sua consulta foi reagendada com sucesso em nosso sistema.</p>
					<p>Clique em OK ou aguarde para continuar.</p>
				</div>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-success" type="button">OK</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
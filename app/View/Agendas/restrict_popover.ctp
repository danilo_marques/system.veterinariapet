<div id="vp-popover-show-scheduler" class="popover" style="top: 349px; left: 500px; display: none; z-index: 1032; min-widht:200px;" aria-hidden="true">
	<div class="arrow"></div>
	<h3 class="popover-title">
		<strong class="vp-popover-time">Horário:</strong>
		<button type="button" class="close vp-close-popover" data-dismiss="modal-popup" aria-hidden="true">&times;</button>
	</h3>

	<div class="popover-content">
		<div class="vp-popover-customer-name"></div>
		<div class="vp-popover-pet-name"></div>
		<div class="vp-popover-customer-phone"></div>
		<div class="vp-popover-reservation-obs"></div>

		<div class="pull-left">
			<a href="javascript:void(0);" id="vp-btn-popover-delete-scheduling" title="Excluir" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
			<a href="javascript:void(0);" id="vp-btn-popover-rescheduling" title="Reagendar" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i></a>
		</div>

		<div class="pull-right">
			<a id="vp-btn-popover-treatment" title="Atender" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Atender</a>
		</div>

		<div class="clearfix"></div>
	</div>
</div>

<div id="vp-show-popover" class="vp-close-popover"></div>
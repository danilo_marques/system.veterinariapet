<!-- Modal -->
<div class="modal fade" id="vp-lightbox-new-scheduling" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="panel-title">Novo agendamento</h2>
            </div>
            <div class="modal-body">
                <?php echo $this->form->hidden('dhtmlx_event_id', array('id' => 'dhtmlx_event_id')); ?>

                <form class="form-horizontal" id="vp-form-add-scheduling" action="/agenda/adicionar" method="post">

                    <?php //echo $this->form->hidden('Reservation.id', array('id' => 'vp-form-add-agenda-reservation-id')); ?>

                    <div class="modal-wrapper">
                        <div class="form-group">
                            <label for="vp-form-add-agenda-select2-customer-name" class="col-sm-3 control-label">Nome do cliente</label>
                            <div class="col-sm-9">
                                <?php echo $this->form->text('Customer.id', array('id' => 'vp-form-add-agenda-select2-customer-name', 'label' => false, 'div' => false)); ?>
                            </div>
                        </div>

                        <div class="form-group hide" id="vp-select2-pet-name">
                            <label for="vp-form-add-agenda-select2-pet-name" class="col-sm-3 control-label">Nome do pet</label>
                            <div class="col-sm-9">
                                <?php echo $this->form->input('Pet.id', array('options' => array(), 'id' => 'vp-form-add-agenda-select2-pet-name', 'label' => false, 'div' => false)); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="vp-form-add-agenda-customer-phone" class="col-sm-3 control-label">Telefone</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                    <?php echo $this->form->text('Customer.phone', array('id' => 'vp-form-add-agenda-customer-phone', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="vp-form-add-agenda-customer-cellphone" class="col-sm-3 control-label">Celular</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-mobile"></i>
                                    </span>
                                    <?php echo $this->form->text('Customer.cellphone', array('id' => 'vp-form-add-agenda-customer-cellphone', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="vp-form-add-agenda-start_date" class="col-sm-3 control-label">Data</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <?php echo $this->form->text('Reservation.start_date', array('id' => 'vp-form-add-agenda-start_date', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="vp-form-add-agenda-start_time" class="col-sm-3 control-label">Horário</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                    <?php echo $this->form->text('Reservation.start_time', array('id' => 'vp-form-add-agenda-start_time', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                </div>
                            </div>

                            <label for="vp-form-add-agenda-treatment_duration" class="col-sm-3 control-label">Duração</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-history"></i>
                                    </span>
                                    <?php echo $this->form->text('Reservation.treatment_duration', array('id' => 'vp-form-add-agenda-treatment_duration', 'value' => $this->Session->read('User.treatment_duration'), 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="vp-form-add-agenda-observation" class="col-sm-3 control-label">Observações</label>
                            <div class="col-sm-9">
                                <?php echo $this->form->textarea('Reservation.observations', array('id' => 'vp-form-add-agenda-observation', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-scheduling" class="btn btn-success">Agendar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="vp-lightbox-delete-scheduling" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="i-circle danger"><i class="fa fa-question"></i></div>
					<h4>Exclusão de consulta</h4>
					<p>Deseja realmente apagar permanentemente essa consulta?</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger" id="vp-btn-confirm-delete-scheduling" data-dismiss="modal">Apagar</button>
			</div>
		</div>
	</div>
</div>
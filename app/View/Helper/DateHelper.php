<?php

class DateHelper extends AppHelper {

	/* Transforma data de nascimento em idade */
	public function birth($date) {	   

		/* Data de nascimento */
	    $birth = new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $date))));
		
	    /* Data atual */
		$current_date = $birth->diff(new DateTime());

		/* Data formatada */
		return $current_date->format('%Y anos e %m meses');
	}

	/* Recebe a data no formato 2014-11-24 23:24:23 e formata para 24/11/2014 */
	public function format_date($date) {
		$new_date = date('d/m/Y', strtotime($date));
		return $new_date;
	}
}
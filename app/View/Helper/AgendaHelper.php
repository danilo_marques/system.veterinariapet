<?php

class AgendaHelper extends AppHelper {

	/* Transforma os números em dias da semana */
	public function weekday($weekdays) {

		$string_weekdays = null;
		$weekdays = explode(',', $weekdays);

		foreach ($weekdays as $key => $weekday) {
			switch ($weekday) {
				case '0':
					$string_weekdays .= 'Domingo, ';
					break;

				case '1':
					$string_weekdays .= 'Segunda, ';
					break;

				case '2':
					$string_weekdays .= 'Terça, ';
					break;

				case '3':
					$string_weekdays .= 'Quarta, ';
					break;

				case '4':
					$string_weekdays .= 'Quinta, ';
					break;

				case '5':
					$string_weekdays .= 'Sexta, ';
					break;

				case '6':
					$string_weekdays .= 'Sábado, ';
					break;
			}
		}

		$string_weekdays = substr($string_weekdays, 0, -2);

		return $string_weekdays;
	}
}
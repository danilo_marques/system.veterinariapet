<div id="page-content">
    <div id="wrap">
        <ol class="breadcrumb">
            <li><a href="/agenda">Agenda</a></li>
            <li>Pets</li>
            <li class="active">Novo cliente</li>
        </ol>
        
        <div class="row">
            <div class="col-sm-12">
                <?php echo $this->Session->flash(); ?>
            </div>
        </div>

        <div class="container">
            <fieldset>
                <legend><strong>Cadastrar novo cliente</strong></legend>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Cadastro</h4>
                    </div>
                    <div class="panel-body">
                        <form action="/pets/novo_cliente" method="post" id="vp-form-wizard-add-customer-and-pet" class="form-horizontal">
                            <fieldset title="1º passo">
                                <legend>Dados do cliente</legend>
                                <div class="form-group">
                                    <label for="fieldname" class="col-md-2 control-label">Nome completo</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Customer.name', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldname" class="col-md-2 control-label">Sexo</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Customer.gender', array('options' => array('Masculino' => 'Masculino', 'Feminino' => 'Feminino'), 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldname" class="col-md-2 control-label">Nascimento</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->text('Customer.birth', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldname" class="col-md-2 control-label">Telefone</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Customer.phone', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldname" class="col-md-2 control-label">Celular</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Customer.cellphone', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldname" class="col-md-2 control-label">Comercial</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Customer.commercial_phone', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldemail" class="col-md-2 control-label">Email</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Customer.email', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset title="2º passo">
                                <legend>Endereço</legend>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">CEP</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Address.zip', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Logradouro</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Address.address', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Número</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Address.number', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Complemento</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Address.complement', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Bairro</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Address.district', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Cidade</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Address.city', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Estado</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Address.state', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset title="3º passo">
                                <legend>Dados do pet</legend>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Nome</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Pet.name', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Sexo</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Pet.gender', array('options' => array('Macho' => 'Macho', 'Fêmea' => 'Fêmea'), 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Nascimento</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->text('Pet.birth', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Tipo de pet</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Pet.pet_type_id', array('options' => $pet_type, 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Raça</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Pet.race', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Porte</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Pet.size', array('options' => array('1' => 'Pequeno porte', '2' => 'Médio porte', '3' => 'Grande porte'), 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fieldnick" class="col-md-2 control-label">Cor dos Pelos</label>
                                    <div class="col-md-6">
                                        <?php echo $this->form->input('Pet.pelage_color', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                                    </div>
                                </div>
                            </fieldset>
                            <input type="submit" class="finish btn-success btn" value="Salvar" />
                        </form>
                    </div>
                </div>

            </fieldset>
        </div>
    </div>
</div>
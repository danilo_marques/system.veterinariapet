<div id="page-content">
    <div id="wrap">
        <ol class="breadcrumb">
            <li><a href="/agenda">Agenda</a></li>
            <li>Usuários do sistema</li>
            <li class="active">Cadastro</li>
        </ol>
        <div class="container">
            <fieldset>
                <legend><strong>Cadastro de usuário</strong></legend>

                <form class="form-horizontal" method="post" action="/usuarios/cadastrar">

                    <?php echo $this->form->hidden('OfficeUnit.OfficeUnitUser.office_unit_id', array('value' => $this->Session->read('User.office_unit_id'))); ?>

                    <div class="form-group">
                        <label for="fieldname" class="col-md-2 control-label">Nome do usuário</label>
                        <div class="col-md-6">
                            <?php echo $this->form->input('User.name', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fieldemail" class="col-md-2 control-label">Tipo de usuário</label>
                        <div class="col-md-3">
                            <?php echo $this->form->input('OfficeUnit.OfficeUnitUser.type', array('options' => array($type), 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fieldemail" class="col-md-2 control-label">Permissão</label>
                        <div class="col-md-3">
                            <?php echo $this->form->input('OfficeUnit.OfficeUnitUser.admin', array('options' => $permission, 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </div>
                    </div>

                    <div class="" id="show-only-to-veterinary">
                        <div class="form-group">
                            <label for="fieldname" class="col-md-2 control-label">Especialidade</label>
                            <div class="col-md-3">
                                <?php echo $this->form->input('Specialty.UserSpecialty.specialty_id', array('options' => $specialties, 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fieldname" class="col-md-2 control-label">Sexo</label>
                        <div class="col-md-3">
                            <?php echo $this->form->input('User.gender', array('options' => array('Masculino' => 'Masculino', 'Feminino' => 'Feminino'), 'empty' => 'Selecionar', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fieldemail" class="col-md-2 control-label">Email</label>
                        <div class="col-md-6">
                            <?php echo $this->form->input('User.email', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fieldurl" class="col-md-2 control-label">Senha</label>
                        <div class="col-md-4">
                            <?php echo $this->form->password('User.password', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fieldurl" class="col-md-2 control-label">Confirmar senha</label>
                        <div class="col-md-4">
                            <?php echo $this->form->password('User.confirm_password', array('class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </div>
                    </div>

                    <!-- <div class="br-1"></div> -->
                    
                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-2">
                            <input type="submit" class="btn btn-primary" value="Salvar">
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
</div>
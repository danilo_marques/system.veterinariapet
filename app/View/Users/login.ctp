<div id="cl-wrapper" class="login-container">
	<div class="middle-login">
		<div class="block-flat">
			<div class="header">							
				<h3 class="text-center"><img class="logo-img" width="25" height="26" src="/img/logo_30x31.png" alt="VeterináriaPet"/>VeterináriaPet</h3>
			</div>
			<div>
				<form class="form-horizontal" action="/login/profissional" method="post">
					<div class="content">
						<h4 class="title">Acesso ao sistema</h4>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<?php echo $this->form->input('User.email', array('class' => 'form-control input-lg', 'placeholder' => 'Digite seu email', 'div' => false, 'label' => false)); ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<?php echo $this->form->password('User.password', array('class' => 'form-control input-lg', 'placeholder' => 'Digite sua senha', 'div' => false, 'label' => false)); ?>
									</div>
								</div>
							</div>
							
					</div>
					<div class="foot">
						<button class="btn btn-default" data-dismiss="modal" type="button">Registrar</button>
						<button class="btn btn-primary" data-dismiss="modal" type="submit">Entrar</button>
					</div>
				</form>
			</div>
		</div>
		<div class="text-center out-links"><a href="javascript:void(0);">&copy; Copyright VeterináriaPet <?php echo date('Y'); ?>. Todos os direitos reservados.</a></div>
	</div>
</div>
<div id="page-content">
    <div id="wrap">
        <ol class="breadcrumb">
            <li><a href="/agenda">Agenda</a></li>
            <li class="active">Usuários do sistema</li>
        </ol>

        <div class="row">
            <div class="col-sm-10 col-md-offset-1">
                <?php echo $this->Session->flash(); ?>
            </div>
        </div>

        <div class="container">
            <fieldset>
                <legend><strong>Usuários do sistema</strong></legend>

                <form class="form-horizontal" method="get" action="/usuarios/busca">
                    <div class="form-group">
                        <label for="fieldname" class="col-md-2 control-label">Nome do usuário</label>
                        <div class="col-md-8">

                            <div class="input-group">
                                <?php echo $this->form->input('User.name', array('class' => 'form-control', 'placeholder' => 'Digite o nome do usuário', 'div' => false, 'label' => false)); ?>
                                <div class="input-group-btn">
                                    <button class="btn btn-info" type="submit">Buscar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                                <a href="/usuarios/cadastrar" class="btn btn-success">Adicionar</a>
                            </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body collapse in">
                                <table class="table table-striped table-bordered datatables">
                                    <thead>
                                        <tr>
                                            <th colspan="4">Seu Perfil</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $user['User']['name']; ?></td>
                                            <td><?php echo $user['User']['phone']; ?></td>
                                            <td><?php echo $user['User']['cellphone']; ?></td>

                                            <td style="text-align:center;">
                                                <a href="/usuarios/editar/<?php echo $user['User']['id']; ?>" class="btn-sm btn-info">Editar</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
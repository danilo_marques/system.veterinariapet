<div id="cl-wrapper" class="fixed-menu">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div id="vp-treatment-templates" class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="block-flat">
                            <div class="content">                       
                                <h3 class="color-primary">Prontuário</h3>

                                <div class="vp-edit-treatment-templates">
                                    <form id="vp-form-edit-treatment-template" class="form-horizontal" action="" method="post">
                                        <fieldset>
                                            <legend><h4>Nome do modelo de atendimento:</h4></legend>

                                            <div class="form-group">
                                                <label class="col-sm-1 control-label" for="vp-form-edit-treatment-template-name">Nome:</label>
                                                <div class="col-sm-4">
                                                    <?php echo $this->form->input('TreatmentTemplate.name', array('value' => $treatment_template['TreatmentTemplate']['name'], 'id' => 'vp-form-edit-treatment-template-name', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <legend><h4>Visualização do modelo de atendimento:</h4></legend>

                                            <div class="btn-add-field">
                                                <button class="btn btn-success btn-sm btn-rad pull-right" type="button"><i class="fa fa-plus-circle"></i> Adicionar novo campo</button>
                                            </div>
                                            <div class="clearfix"></div>

                                            <?php foreach($treatment_fields as $key => $field): ?>
                                                <div class="form-group dashed-border">
                                                    <a class="btn btn-rad btn-danger pull-right" href="#"><i class="fa fa-close"></i></a>

                                                    <a class="btn btn-rad btn-primary pull-right" href="#"><i class="fa fa-pencil"></i></a>

                                                    <label class="col-sm-2 control-label" for="vp-form-edit-treatment-template-name"><?php echo $field['TreatmentField']['field_label']; ?></label>
                                                    <div class="<?php echo "col-md-" . $field['TreatmentField']['field_size']; ?>">
                                                        <?php echo $this->form->$field['TreatmentField']['field_type']('TreatmentTemplate.'.$field['TreatmentField']['field_name'], array('id' => 'vp-form-edit-treatment-template-name', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </fieldset>

                                            <button class="btn btn-success btn-rad pull-right" type="button"> Salvar</button>
                                            <a class="btn btn-rad btn-default pull-left" href="#"><i class="fa fa-trash"> Excluir</i></a>
                                            <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
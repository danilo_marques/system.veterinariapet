<div id="cl-wrapper" class="fixed-menu">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div id="vp-treatment" class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="block-flat">
                            <div class="content">

                                <?php echo $this->Html->image('image_pets/'.$pet['Pet']['avatar'], array('class' => 'pet-avatar', 'alt' => $pet['Pet']['name'], 'width' => '100')); ?>

                                <div class="vp-treatment-data-pet">
                                    <h4 class="color-primary">
                                        <strong><?php echo $pet['Pet']['name']; ?></strong>
                                    </h4>

                                    <button class="btn btn-success btn-sm btn-rad pull-right" id="vp-btn-init-treatment" type="button"><i class="fa fa-play"></i> Iniciar atendimento</button>

                                    <p class="pet-information">
                                        <strong>Idade: </strong><?php echo $this->Date->birth($pet['Pet']['birth']); ?>
                                    </p>
                                    <p class="pet-information">
                                        <?php if(count($treatments) == 0): ?>
                                            <span class="color-success"><b>Primeira consulta</b></span>
                                        <?php else: ?>
                                            <span>
                                                <strong>Primeira consulta em:</strong> <?php echo $this->Date->format_date($treatments[0]['Treatment']['created']); ?>, <?php echo count($treatments); ?> <?php echo count($treatments) > 1 ? 'atendimentos' : 'atendimento'; ?>
                                            </span>
                                        <?php endif; ?>
                                    </p>
                                </div>

                                <?php if(count($treatments) == 0): ?>
                                    <p class="help-text">
                                        Este pet não possui nenhum registro de atendimento anterior.<br>
                                        Para iniciar um atendimento, clique no botão
                                        <b class="color-success"> Iniciar atendimento</b> 
                                        no lado direito da tela.
                                    </p>
                                <?php endif; ?>

                                <form id="vp-form-init-treatment" class="form-horizontal" action="/atendimento/salvar" method="post">
                                    <?php echo $this->form->hidden('Treatment.pet_id', array('value' => $pet['Pet']['id'])); ?>

                                    <?php echo $this->form->hidden('Treatment.office_unit_user_id', array('value' => $this->Session->read('User.office_unit_user_id'))); ?>

                                    <?php echo $this->form->hidden('Treatment.treatment_template_id', array('value' => 1)); ?>

                                    <?php echo $this->form->hidden('Treatment.status', array('value' => 1)); ?>

                                    <div class="tab-container tab-left vp-tabs-treatment">
                                        <ul class="nav nav-tabs flat-tabs">
                                            <?php foreach($treatment_templates as $key => $treatment_template) : ?>
                                                <li class="<?php echo $treatment_template['TreatmentTemplate']['default'] == 1 ? "active" : ""; ?>">
                                                    <a data-toggle="tab" href="#treatment-<?php echo $key; ?>"><?php echo $treatment_template['TreatmentTemplate']['name']; ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php foreach($treatment_templates as $key => $treatment_template) : ?>
                                                <div id="treatment-1" class="tab-pane cont fade <?php echo $treatment_template['TreatmentTemplate']['default'] == 1 ? "active in" : ""; ?>">
                                                    <?php foreach($treatment_fields as $key => $treatment_field) : ?>
                                                        <div class="form-group">
                                                            
                                                            <label class="col-sm-2 control-label" for="vp-form-edit-treatment-template-name"><?php echo $treatment_field['TreatmentField']['field_label']; ?></label>
                                                            
                                                            <div class="<?php echo "col-md-" . $treatment_field['TreatmentField']['field_size']; ?>">

                                                                <?php echo $this->form->hidden('TreatmentValue.'.$key.'.treatment_field_id', array('value' => $treatment_field['TreatmentField']['id'])); ?>

                                                                <?php echo $this->form->$treatment_field['TreatmentField']['field_type']('TreatmentValue.'.$key.'.value', array('id' => 'vp-form-edit-treatment-template-name', 'class' => 'form-control', 'label' => false, 'div' => false)); ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <button class="btn btn-success btn-rad pull-right" type="submit"><i class="fa fa-save"></i> Finalizar atendimento</button>

                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>

                        <div class="row dash-cols">
                            <?php foreach($treatments as $treatment): ?>
                                <div class="col-sm-12 col-md-12">
                                    <div class="block">
                                        <div class="header">
                                            <h2>
                                                <b><?php echo $treatment['TreatmentTemplate']['name']; ?></b>
                                                <span class="pull-right">
                                                    <small>
                                                        <strong><?php echo $this->Date->format_date($treatment['Treatment']['created']); ?></strong>
                                                    </small>
                                                </span>
                                            </h2>
                                            <h3><span data-placement="top" data-toggle="tooltip" data-original-title="Responsável pelo atendimento"><?php echo $treatment['OfficeUnitUser']['User']['name']; ?></span></h3>
                                        </div>
                                        <div class="content no-padding ">
                                            <ul class="items">
                                                <?php foreach($treatment['TreatmentTemplate']['TreatmentField'] as $key => $treatment_field) : ?>
                                                    <li>
                                                        <i class="fa fa-file-text"></i><strong><?php echo $treatment_field['field_label']; ?></strong>
                                                        <small><?php echo $treatment['TreatmentValue'][$key]['value']; ?></small>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <div class="total-data bg-blue">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                                                <h2>Ações <b class="caret"></b></h2>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-pencil-square-o"></i>&nbsp; Inserir informações</a></li>
                                                <li><a href="#"><i class="fa fa-print"></i>&nbsp; Imprimir prontuário</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
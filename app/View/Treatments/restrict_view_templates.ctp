<div id="cl-wrapper" class="fixed-menu">
    <?php echo $this->element('menu'); ?>

    <div id="pcont" class="container-fluid">
        <div class="cl-mcont">
            <?php echo $this->Session->flash(); ?>

            <div id="vp-treatment-templates" class="col-sm-12 col-md-12 col-lg-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="block-flat">
                            <div class="content">
                                <div class="header">                          
                                    <h3>Prontuários</h3>
                                </div>

                                <div class="vp-view-treatment-templates">
                                    <div class="col-sm-12 col-md-12">
                                        <button class="btn btn-success btn-sm btn-rad pull-right" type="button"><i class="fa fa-plus-circle"></i> Adicionar</button>

                                        <div class="input-group col-sm-1 select-all-checkbox">
                                            <span class="input-group-addon">
                                                <input type="checkbox" class="ckb_select_all" ckb_select="select_all">
                                            </span>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ações <span class="caret"></span></button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#">Excluir selecionados</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="col-sm-12 col-md-12">
                                        <?php foreach($templates as $key => $template): ?>
                                            <div class="checkbox">
                                                <label>
                                                    <?php echo $this->form->checkbox('TreatmentTemplate.id', array('class' => 'select_all', 'value' => $template['TreatmentTemplate']['id'], 'hiddenField' => false, 'label' => false)); ?>
                                                    <a href="/configuracoes/prontuarios/<?php echo $template['TreatmentTemplate']['id']; ?>"><?php echo $template['TreatmentTemplate']['name']; ?></a>
                                                </label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
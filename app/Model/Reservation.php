<?php

class Reservation extends AppModel {

	public $name = 'Reservation';
	public $useTable = 'reservations';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'dependent' => true
		),
		'OfficeUnit' => array(
			'className' => 'OfficeUnit',
			'foreignKey' => 'office_unit_id',
			'dependent' => true
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'dependent' => true
		),
		'Pet' => array(
			'className' => 'Pet',
			'foreignKey' => 'pet_id',
			'dependent' => true
		)
	);
}
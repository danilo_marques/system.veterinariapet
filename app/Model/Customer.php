<?php

class Customer extends AppModel {

	public $name = 'Customer';
	public $useTable = 'customers';

	public $actsAs = array('Containable');

	public $hasAndBelongsToMany = array(
    	'Pet' => array(
            'className' => 'Pet',
            'joinTable' => 'customer_pets',
            'foreignKey' => 'customer_id',
            'associationForeignKey' => 'pet_id',
            'unique' => true,
        ),
        'OfficeUnit' => array(
            'className' => 'OfficeUnit',
            'joinTable' => 'office_unit_customers',
            'foreignKey' => 'customer_id',
            'associationForeignKey' => 'office_unit_id'
        )
    );

    public $belongsTo = array(
        'Address' => array(
            'className' => 'Address',
            'foreignKey' => 'address_id',
            'dependent' => true
        )
    );

    public $hasMany = array(
        'Resevation' => array(
            'className' => 'Resevation',
            'foreignKey' => 'reservation_id',
            'dependent' => true
        )
    );
}
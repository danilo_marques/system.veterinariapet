<?php

class Office extends AppModel {

	public $name = 'Office';
	public $useTable = 'offices';

	public $actsAs = array('Containable');

	public $hasMany = array(
		'OfficeUnit' => array(
			'className' => 'OfficeUnit',
			'foreignKey' => 'office_unit_id',
			'dependent' => true
		)
	);
}
<?php

class OfficeUnit extends AppModel {

	public $name = 'OfficeUnit';
	public $useTable = 'office_units';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Office' => array(
			'className' => 'Office',
			'foreignKey' => 'office_id',
			'dependent' => true
		),
		'Address' => array(
			'className' => 'Address',
			'foreignKey' => 'address_id',
			'dependent' => true
		)
	);

	public $hasAndBelongsToMany = array(
        'Customer' => array(
            'className' => 'Customer',
            'joinTable' => 'office_unit_customers',
            'foreignKey' => 'office_unit_id',
            'associationForeignKey' => 'customer_id'
        )
    );

	public $hasMany = array(
        'AgendaConfiguration' => array(
            'className' => 'AgendaConfiguration',
            'foreignKey' => 'office_unit_id',
            'dependent' => true
        )
    );
}
<?php

class TreatmentTemplate extends AppModel {

	public $name = 'TreatmentTemplate';
	public $useTable = 'treatment_templates';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'OfficeUnitUser' => array(
			'className' => 'OfficeUnitUser',
			'foreignKey' => 'office_unit_user_id',
			'dependent' => true
		)
	);

	public $hasMany = array(
		'TreatmentField' => array(
			'className' => 'TreatmentField',
			'dependent' => true
		),
		'Treatment' => array(
			'className' => 'Treatment',
			'dependent' => true
		)
	);
}
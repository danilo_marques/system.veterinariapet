<?php

class AgendaConfiguration extends AppModel {

	public $name = 'AgendaConfiguration';
	public $useTable = 'agenda_configurations';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'dependent' => true
		),
		'OfficeUnit' => array(
			'className' => 'OfficeUnit',
			'foreignKey' => 'office_unit_id',
			'dependent' => true
		)
	);
}
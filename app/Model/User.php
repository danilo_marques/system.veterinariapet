<?php

App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel {

	public $name = 'User';
	public $useTable = 'users';

	public $actsAs = array('Containable');

    public $hasAndBelongsToMany = array(
    	'Specialty' => array(
            'className' => 'Specialty',
            'joinTable' => 'user_specialties',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'specialty_id'
        ),
        'OfficeUnit' => array(
            'className' => 'OfficeUnit',
            'joinTable' => 'office_unit_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'office_unit_id'
        )
    );

    public $hasMany = array(
        'AgendaConfiguration' => array(
            'className' => 'AgendaConfiguration',
            'foreignKey' => 'user_id',
            'dependent' => true
        )
    );

    /* Encripta a senha antes de salvar no banco */
    public function beforeSave($options = array()) {
	    if (isset($this->data['User']['password'])) {
	        $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
	    }
	    return true;
	}

	/* Enum tipo de usuário */
	const VETERINARY = 1;
    const SECRETARY = 0;

    /* Enum permissão do usuário */
    const ADMIN = 1;
    const NORMAL = 0;

    /* Enum Pefil online */
    const ONLINE = 1;
    const OFFLINE = 0;

    public $enum = array(
        'type' => array(
            self::VETERINARY => 'Veterinário',
            self::SECRETARY => 'Secretária'
        ),
        'permission' => array(
            self::ADMIN => 'Administrador',
            self::NORMAL => 'Normal'
        ),
        'online' => array(
            self::ONLINE => 'Online',
            self::OFFLINE => 'Offiline'
        )
    );
}
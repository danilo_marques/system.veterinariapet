<?php

class CustomerPet extends AppModel {

	public $name = 'CustomerPet';
	public $useTable = 'customer_pets';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Pet' => array(
			'className' => 'Pet',
			'foreignKey' => 'pet_id',
			'dependent' => true
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'dependent' => true
		)
	);
}
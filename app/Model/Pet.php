<?php

class Pet extends AppModel {

	public $name = 'Pet';
	public $useTable = 'pets';

	public $actsAs = array('Containable');

	public $hasAndBelongsToMany = array(
    	'Customer' => array(
            'className' => 'Customer',
            'joinTable' => 'customer_pets',
            'foreignKey' => 'pet_id',
            'associationForeignKey' => 'customer_id',
            'unique' => true,
        )
    );

    public $hasMany = array(
        'Resevation' => array(
            'className' => 'Resevation',
            // 'foreignKey' => 'reservation_id',
            'dependent' => true
        ),
        'Treatment' => array(
            'className' => 'Treatment',
            'dependent' => true
        )
    );

    // public $hasOne = array(
    //     'PetType' => array(
    //         'className' => 'PetType',
    //         'foreignKey' => 'pet_type_id',
    //         'dependent' => true
    //     )
    // );
}
<?php

App::uses('Model', 'Model');

class AppModel extends Model {

	/* Plugin ENUM */
	public $actsAs = array('Utility.Enumerable');
}

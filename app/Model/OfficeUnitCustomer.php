<?php

class OfficeUnitCustomer extends AppModel {

	public $name = 'OfficeUnitCustomer';
	public $useTable = 'office_unit_customers';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'OfficeUnit' => array(
			'className' => 'OfficeUnit',
			'foreignKey' => 'office_unit_id',
			'dependent' => true
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'dependent' => true
		)
	);
}
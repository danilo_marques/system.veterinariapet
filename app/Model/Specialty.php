<?php

class Specialty extends AppModel {

	public $name = 'Specialty';
	public $useTable = 'specialties';

	public $actsAs = array('Containable');

	public $hasAndBelongsToMany = array(
    	'User' => array(
    		'className' => 'User',
    		'joinTable' => 'user_specialties',
    		'foreignKey' => 'specialty_id',
    		'associationForeignKey' => 'user_id'
    	)
    );
}
<?php

class Address extends AppModel {

	public $name = 'Address';
	public $useTable = 'addresses';

	public $actsAs = array('Containable');

	public $hasOne = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'address_id',
			'dependent' => true
		),
		'OfficeUnit' => array(
			'className' => 'OfficeUnit',
			'foreignKey' => 'office_uni_id',
			'dependent' => true
		)
	);
}
<?php

class TreatmentField extends AppModel {

	public $name = 'TreatmentField';
	public $useTable = 'treatment_fields';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'TreatmentTemplate' => array(
			'className' => 'TreatmentTemplate',
			'foreignKey' => 'treatment_template_id',
			'dependent' => true
		)
	);

	public $hasMany = array(
		'TreatmentValue' => array(
			'className' => 'TreatmentValue',
			'dependent' => true
		)
	);
}
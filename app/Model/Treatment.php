<?php

class Treatment extends AppModel {

	public $name = 'Treatment';
	public $useTable = 'treatments';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Pet' => array(
			'className' => 'Pet',
			'foreignKey' => 'pet_id',
			'dependent' => true
		),
		'OfficeUnitUser' => array(
			'className' => 'OfficeUnitUser',
			'foreignKey' => 'office_unit_user_id',
			'dependent' => true
		),
		'TreatmentTemplate' => array(
			'className' => 'TreatmentTemplate',
			'foreignKey' => 'treatment_template_id',
			'dependent' => true
		)
	);

	public $hasMany = array(
		'TreatmentValue' => array(
			'className' => 'TreatmentValue',
			'dependent' => true
		)
	);
}
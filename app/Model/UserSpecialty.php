<?php

class UserSpecialty extends AppModel {

	public $name = 'UserSpecialty';
	public $useTable = 'user_specialties';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'dependent' => true
		),
		'Specialty' => array(
			'className' => 'Specialty',
			'foreignKey' => 'specialty_id',
			'dependent' => true
		)
	);
}
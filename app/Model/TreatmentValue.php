<?php

class TreatmentValue extends AppModel {

	public $name = 'TreatmentValue';
	public $useTable = 'treatment_values';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'TreatmentField' => array(
			'className' => 'TreatmentField',
			'foreignKey' => 'treatment_field_id',
			'dependent' => true
		),
		'Treatment' => array(
			'className' => 'Treatment',
			'foreignKey' => 'treatment_id',
			'dependent' => true
		)
	);
}
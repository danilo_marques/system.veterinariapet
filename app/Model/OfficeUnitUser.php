<?php

class OfficeUnitUser extends AppModel {

	public $name = 'OfficeUnitUser';
	public $useTable = 'office_unit_users';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'OfficeUnit' => array(
			'className' => 'OfficeUnit',
			'foreignKey' => 'office_unit_id',
			'dependent' => true
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'dependent' => true
		)
	);

	public $hasMany = array(
		'Treatment' => array(
			'className' => 'Treatment',
			'dependent' => true
		),
		'TreatmentTemplate' => array(
			'className' => 'TreatmentTemplate',
			'dependent' => true
		)
	);
}
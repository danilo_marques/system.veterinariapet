<?php

class PetType extends AppModel {

	public $name = 'PetType';
	public $useTable = 'pet_types';

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Pet' => array(
			'className' => 'Pet',
			'foreignKey' => 'pet_id',
			'dependent' => true
		)
	);
}
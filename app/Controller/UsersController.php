<?php

App::uses('AuthComponent', 'Controller/Component');

class UsersController extends AppController {
	public $uses = array('User', 'Specialty', 'OfficeUnit', 'OfficeUnitUser');

	public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'logout');

        /* Enum */
        $type = $this->User->enum('type');
        $permission = $this->User->enum('permission');
        $online = $this->User->enum('online');

        $this->set(compact('type', 'permission', 'online'));
    }

	public function restrict_index() {
		$user = $this->User->find('first', array(
				'conditions' => array('User.id' => CakeSession::read('Auth.User.id')),
				'recursive' => 0
			)
		);

		$this->set(compact('user'));
	}

	public function restrict_add() {
		if($this->data) {
			// debug($this->data);die;
			if($this->User->saveAll($this->data)) {
				$this->Session->setFlash('Usuário cadastrado com sucesso', 'success');
			} else {
				$this->Session->setFlash('Ocorreu um erro ao cadastrar o usuário, tente novamente!', 'error');
			}

			$this->redirect('/usuarios');
		}

		$specialties = $this->Specialty->find('list', array(
				'fields' => array('id', 'name'),
				'conditions' => array('Specialty.deleted' => null),
				'recursive' => 0
			)
		);

		$this->set(compact('specialties'));
	}

	public function restrict_view() {

	}

	public function restrict_edit() {

	}

	public function restrict_search() {

	}

	public function login() {
		$this->layout = 'login';

		if($this->data){
            if ($this->Auth->login()) {
            	if($this->Session->read('Auth.User.first_access') == 0) {
            		$this->redirect('/agenda');
            	} else {
            		$this->redirect('/primeiro-acesso/configuracoes');
            	}
            } else {
                $this->Session->setFlash('Email ou senha incorreto!', 'error');
            }   
        }
	}

	public function logout() {
		$this->Session->delete('User');
		$this->redirect($this->Auth->logout());
	}
}
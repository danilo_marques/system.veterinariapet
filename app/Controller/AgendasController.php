<?php

class AgendasController extends AppController {

	public $uses = array('Agenda', 'Reservation');
	public $helpers = array('Agenda');

	public function restrict_index() {
		$this->layout = 'agenda';
	}

	public function restrict_scheduling_lightbox() {
		$this->layout = false;
	}

	/* Carrega as reservas do usuário */
	public function restrict_load() {
		$this->layout = 'ajax';
        $this->autoRender = false;

		$reservations = $this->Reservation->find('all', array(
	        	'contain' => array('Customer', 'Pet'),
	        	'recursive' => 0,
	            'conditions' => array(
	            	'Reservation.user_id' => $this->Session->read('Auth.User.id'),
	            	'Reservation.office_unit_id' => $this->Session->read('User.office_unit_id'),
	            	'Reservation.status' => 1
            	)
            )
        );

        foreach ($reservations as $key => $reservation) {
        	$reservations[$key]['Reservation']['customer_name'] = $reservation['Customer']['name'];
        	$reservations[$key]['Reservation']['phone'] = $reservation['Customer']['phone'];
        	$reservations[$key]['Reservation']['pet_name'] = $reservation['Pet']['name'];
        }

        $reservations = Set::extract('/Reservation/.', $reservations);

        /* Dhtmlx Connector */
        App::import('Vendor', 'JSONSchedulerConnector', array('file' => 'dhtmlx_connector' . DS . 'scheduler_connector.php'));
        $scheduler = new JSONSchedulerConnector();
        $scheduler->render_array($reservations, "id", "start_date, end_date, pet_name, pet_id, customer_name, event_type, phone, observations");
	}

	/* Salva um agendamento */
	public function restrict_add() {
		if($this->request->is('ajax')){

			$this->layout = 'ajax';
        	$this->autoRender = false;

			$start_date = explode('/', $this->data['Reservation']['start_date']);
			$start_date = $start_date[2].'-'.$start_date[1].'-'.$start_date[0];

			$start_time = $this->data['Reservation']['start_time'];
			$end_time = date('H:i', strtotime('+'.$this->data['Reservation']['treatment_duration'].' Minutes', strtotime($start_time)));

			$this->request->data['Reservation'] = array(
				'user_id' => $this->Session->read('Auth.User.id'),
				'office_unit_id' => $this->Session->read('User.office_unit_id'),
				'pet_id' => $this->data['Pet']['id'],
				'start_date' => $start_date . ' ' . $start_time,
				'end_date' => $start_date . ' ' . $end_time,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'observations' => $this->data['Reservation']['observations']
			);

			if($this->Reservation->saveAll($this->data)) {
				return json_encode(array('status' => 'success'));
			} else {
				return json_encode(array('status' => 'error'));
			}
		} else {
			$this->redirect('/agenda');
		}
	}

	public function restrict_edit() {
		if($this->request->is('ajax')) {

			$this->layout = 'ajax';
        	$this->autoRender = false;

			$start_date = explode('/', $this->data['Reservation']['start_date']);
			$start_date = $start_date[2].'-'.$start_date[1].'-'.$start_date[0];

			$start_time = $this->data['Reservation']['start_time'];
			$end_time = date('H:i', strtotime('+'.$this->data['Reservation']['treatment_duration'].' Minutes', strtotime($start_time)));

			$this->request->data['Reservation'] = array(
				'id' => $this->data['Reservation']['id'],
				'start_date' => $start_date . ' ' . $start_time,
				'end_date' => $start_date . ' ' . $end_time,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'observations' => $this->data['Reservation']['observations']
			);

			if($this->Reservation->saveAll($this->data)) {
				return json_encode(array('status' => 'success'));
			} else {
				return json_encode(array('status' => 'error'));
			}
		} else {
			$this->redirect('/agenda');
		}
	}

	public function restrict_get_reservation_by_id() {
		if($this->request->is('ajax')) {

			$this->layout = 'ajax';
        	$this->autoRender = false;

        	$reservation = $this->Reservation->find('first', array(
        			'contain' => array('Customer', 'Pet'),
        			'recursive' => 0,
        			'conditions' => array(
        				'Reservation.id' => $this->request->query('id'),
        				'Reservation.status' => 1
        			)
        		)
        	);

        	if(count($reservation) > 0) {
        		return json_encode(array('status' => 'success', 'event' => $reservation));
        	} else {
        		return json_encode(array('status' => 'error'));
        	}
		} else {
			$this->redirect('/agenda');
		}
	}
}
<?php

class PetsController extends AppController {

	public $uses = array('Pet', 'Customer', 'CustomerPet', 'PetType', 'Address', 'OfficeUnitCustomer');

	public function restrict_view($id) {
		if(!empty($id)) {
			$pet = $this->Pet->find('first', array(
					'contain' => array('Customer'),
					'conditions' => array(
						'Pet.id' => $id,
						'Pet.status' => 1
					),
					'recursive' => 0
				)
			);

			$pet_types = $this->PetType->find('list', array(
					'fields' => array('PetType.id', 'PetType.name'),
					'conditions' => array('PetType.status' => 1)
				)
			);

			$this->set(compact('pet', 'pet_types'));
		} else {
			$this->redirect('/agenda');
		}
	}

	public function restrict_add() {
		$this->layout = 'register_pet';
		
		if($this->data) {
			if($this->Address->save($this->data['Address'])) {
				$this->request->data['Customer']['address_id'] = $this->Address->getInsertID();
				if($this->Customer->save($this->data['Customer'])) {

					$this->request->data['OfficeUnitCustomer'] = array(
						'office_unit_id' => $this->Session->read('User.office_unit_id'),
						'customer_id' => $this->Customer->getInsertID()
					);

					if($this->OfficeUnitCustomer->save($this->data['OfficeUnitCustomer'])) {
						if($this->Pet->save($this->data['Pet'])) {
							$this->request->data['CustomerPet']['pet_id'] = $this->Pet->getInsertID();
							$this->request->data['CustomerPet']['customer_id'] = $this->Customer->getInsertID();
							if($this->CustomerPet->save($this->data['CustomerPet'])) {
								$this->Session->setFlash('Pet cadastrado com sucesso!', 'success');
								$saved = true;
							}
						}
					}
				}
			}

			if(!isset($saved)) {
				$this->Session->setFlash('Ocorreu um erro ao cadastrar o pet, tente novamente', 'error');
			}
		}

		$pet_type = $this->PetType->find('list', array(
				'fields' => array('PetType.id', 'PetType.name'),
				'conditions' => array('PetType.status' => 1)
			)
		);

		$this->set(compact('pet_type'));
	}

	public function restrict_autocomplete() {
		$this->layout = false;
        $this->autoRender = false;

        if($this->RequestHandler->isAjax()) {
            $pets = $this->Pet->find('all', array(
            		'contain' => array('CustomerPet.customer_id = ' . $this->request->query['customer_id']),
	                'conditions' => array(
	                    'Pet.name LIKE' => '%' . $this->request->query['name'] . '%',
	                ),
	                'recursive' => 0
                )
            );
           
            $pets = Set::extract('/Pet/.', $pets);

            return json_encode($pets);
        }
	}

	public function restrict_search() {
		if($this->request->query) {
			if($this->request->query('data.find_by') == 'pet') {
				$pets = $this->Pet->find('all', array(
		                'contain' => array('Customer'),
		                'conditions' => array(
		                    'Pet.name LIKE' => '%' . $this->request->query('data.name') . '%',
		                ),
		                'recursive' => 0
	                )
	            );

				$this->request->data['name'] = $this->request->query('data.name');
	            $this->set(compact('pets'));
			} else {
				$customers = $this->Customer->find('all', array(
		                'contain' => array('Pet'),
		                'conditions' => array(
		                    'Customer.name LIKE' => '%' . $this->request->query('data.name') . '%',
		                ),
		                'recursive' => 0
	                )
	            );

	            $this->request->data['find_by'] = 'customer';
	            $this->request->data['name'] = $this->request->query('data.name');
	            $this->set(compact('customers'));
			}
		}
	}

	public function restrict_get_by_customer_id() {
		$this->layout = false;
        $this->autoRender = false;

		if($this->data) {

			/* Pelo ID do customer, pego todos os IDs dos Pets pertencentes a ele */
			$pets_ids = $this->CustomerPet->find('list', array(
					'fields' => array('CustomerPet.pet_id'),
					'conditions' => array(
						'CustomerPet.customer_id' => $this->data['customer_id'],
					),
				)
			);

			/* Busco todos os pets pelos IDs */
			$pets = $this->Pet->find('list', array(
					'fields' => array('Pet.id', 'Pet.name'),
					'conditions' => array(
						'Pet.id' => $pets_ids,
					),
					'recursive' => 0
				)
			);

			return json_encode($pets);
		}
	}
}
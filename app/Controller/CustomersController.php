<?php

class CustomersController extends AppController {

	public $uses = array('Customer', 'Pet', 'PetType', 'Address', 'CustomerPet', 'OfficeUnitCustomer');

	public function restrict_add() {
		if($this->data) {

			/* ARRUMAR - Tentar colocar saveAll($this->data) */
			if($this->Address->save($this->data['Address'])) {
				$this->request->data['Customer']['address_id'] = $this->Address->getInsertID();
				if($this->Customer->save($this->data['Customer'])) {

					$this->request->data['OfficeUnitCustomer'] = array(
						'office_unit_id' => $this->Session->read('User.office_unit_id'),
						'customer_id' => $this->Customer->getInsertID()
					);

					if($this->OfficeUnitCustomer->save($this->data['OfficeUnitCustomer'])) {
						if($this->Pet->save($this->data['Pet'])) {
							$this->request->data['CustomerPet']['pet_id'] = $this->Pet->getInsertID();
							$this->request->data['CustomerPet']['customer_id'] = $this->Customer->getInsertID();
							if($this->CustomerPet->save($this->data['CustomerPet'])) {
								$this->Session->setFlash('Cliente cadastrado com sucesso!', 'success');
								$saved = true;
							}
						}
					}
				}
			}

			if(!isset($saved)) {
				$this->Session->setFlash('Ocorreu um erro ao cadastrar o cliente, tente novamente', 'error');
			}

			$this->redirect('/pets/novo_cliente');
		}

		$pet_type = $this->PetType->find('list', array(
				'fields' => array('PetType.id', 'PetType.name'),
				'conditions' => array('PetType.status' => 1)
			)
		);

		$this->set(compact('pet_type'));
	}

	public function restrict_autocomplete() {
		$this->layout = false;
        $this->autoRender = false;

        if($this->RequestHandler->isAjax()) {
            $customers = $this->Customer->find('all', 
            	array(
	                'conditions' => array(
	                    'Customer.name LIKE' =>  '%' . $this->request->query['name'] . '%',
	                ),
	                'recursive' => 0
                )
            );

            $customers = Set::extract('/Customer/.', $customers);

            return json_encode($customers);
        }
	}
}
<?php

class AgendaConfigurationsController extends AppController {

	public $uses = array('AgendaConfiguration', 'OfficeUnitUser');

	/* Carrega as configurações da agenda do usuário */
	public function restrict_set_config() {
		$this->layout = 'ajax';
        $this->autoRender = false;

        $configs = $this->AgendaConfiguration->find('all', array(
	            'conditions' => array(
	            	'AgendaConfiguration.user_id' => $this->Session->read('Auth.User.id'),
	            	'AgendaConfiguration.office_unit_id' => $this->Session->read('User.office_unit_id'),
	            	'AgendaConfiguration.status' => 1
            	),
            	'recursive' => 1,
            )
        );

        $configs = Set::extract('/AgendaConfiguration/.', $configs);

        /* Início: Verifica todos os dias foram configurados */
        $weekdays = array();
        $all_weekdays = array();

        foreach ($configs as $config) {
        	$weekdays = explode(',', $config['weekday']);

        	foreach ($weekdays as $weekday) {
        		$all_weekdays[] = $weekday;
        	}
        }

        for($day = 0; $day < 7; $day++) {
        	$day_exist = false;
        	foreach ($all_weekdays as $one_weekday) {
	        	if($one_weekday == $day) {
	        		$day_exist = true;
	        	}
	        }

	        // Caso o dia não tenha cido configurado, seto ele com bloqueado
	        if($day_exist == false) {
	        	$configs[count($configs)] = array(
	        		'start_time' => '00:00',
					'end_time' => '00:00',
					'start_time_lunch' => '00:00',
					'end_time_lunch' => '00:00',
					'weekday' => "$day",
	        	);
	        }
        }
        /* Fim: Verifica todos os dias foram configurados */

        return json_encode($configs);
	}

	/* Salva as configurações da agenda */
	public function restrict_add() {
		if($this->data) {

			$weekday = null;
			foreach ($this->data['Agenda'] as $key => $value) {
				$weekday .= $value['weekday'] . ',';
			}

			unset($this->request->data['Agenda']);
			$this->request->data['AgendaConfiguration']['weekday'] = substr($weekday, 0, -1);

			if($this->AgendaConfiguration->saveAll($this->data)) {
				$this->OfficeUnitUser->saveAll($this->data);
				$this->Session->setFlash('Horário Configurado com sucesso!', 'success');
			} else {
				$this->Session->setFlash('Ocorreu um erro ao configurar o horário, tente novamente!', 'error');
			}

			if(!isset($this->data['first_access'])) {
				$this->redirect('/configuracoes/agenda');
			} else {
				$this->redirect('/agenda');
			}
		}

		$configurations = $this->AgendaConfiguration->find('all', array(
				'conditions' => array(
					'AgendaConfiguration.user_id' => $this->Session->read('Auth.User.id'),
					'AgendaConfiguration.office_unit_id' => $this->Session->read('User.office_unit_id'),
					'AgendaConfiguration.status' => 1
				)
			)
		);

		$this->set(compact('configurations'));
	}

	public function restrict_delete() {
		if ($this->request->is('ajax')) {
			$this->layout = 'ajax';
			$this->autoRender = false;

			if($this->data['action'] == 'delete-this') {

				$data['AgendaConfiguration'] = array(
					'id' => $this->data['agenda_configuration_id'],
					'status' => 0,
					'deleted' => date('Y-m-d H:i:s')
				);

				if($this->AgendaConfiguration->saveAll($data)) {
					$this->Session->setFlash('Horário apagado com sucesso!', 'success');
					return json_encode(array('status' => 'success'));
				} else {
					$this->Session->setFlash('Ocorreu um erro ao apagar o horário, tente novamente!', 'error');
					return json_encode(array('status' => 'error'));
				}
			} else {
				$delete_all = $this->AgendaConfiguration->updateAll(
					array(
						'AgendaConfiguration.status' => 0,
						'AgendaConfiguration.deleted' => "'".date('Y-m-d H:i:s')."'"
					),
					array(
						'AgendaConfiguration.user_id' => $this->Session->read('Auth.User.id'),
						'AgendaConfiguration.office_unit_id' => $this->Session->read('User.office_unit_id')
					), 
					false
				);

				if($delete_all == true) {
					$this->Session->setFlash('Horários apagados com sucesso!', 'success');
					return json_encode(array('status' => 'success'));
				} else {
					$this->Session->setFlash('Ocorreu um erro ao apagar o horários, tente novamente!', 'error');
					return json_encode(array('status' => 'error'));
				}
			}
		} else {
			$this->redirect('/configuracoes/agenda');
		}
	}

	/* Salva os bloqueios de horários */
	public function restrict_blockade() {

	}

	/* Configurações do primero acesso */
	public function restrict_first_access() {
		$this->layout = 'first_access';
	}
}
<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $uses = array('Office', 'User', 'OfficeUnitUser');

	public $helpers = array('Html', 'Form', 'Session', 'Js');

	public $components = array(
        'DebugKit.Toolbar',
        'Paginator',
        'Session', 
        'RequestHandler',
        'Auth' => array(
	        'authenticate' => array(
	            'Form' => array(
	                'fields' => array('username' => 'email')
	            )
	        ),
            'loginRedirect' => array('controller' => 'Agendas', 'action' => 'index', 'restrict' => true),
            'logoutRedirect' => '/login/profissional',
            'scope' => array(
            	'User.status' => '1',
            	'User.deleted' => null
            )
        ),
        'Utility.AutoLogin' => array(
            'cookieName' => 'vpRememberMe',
            'expires' => '+1 week',
            'requirePrompt' => false
        )
    );

    public function beforeFilter() {

        /* Aqui verifica se está logado - MELHORAR */
        if (isset($this->params['restrict']) && empty($this->Auth->user('id'))) {
            $this->redirect('/login/profissional');
        }

        if($this->Session->read('Auth.User')) {

            $user = $this->OfficeUnitUser->find('first', array(
                    'conditions' => array('OfficeUnitUser.user_id' => $this->Session->read('Auth.User.id')),
                    'contain' => array('User', 'OfficeUnit' => array('Office'))
                )
            );

            $this->Session->write('User.user_type', $user['User']['type']);
            $this->Session->write('User.office_id', $user['OfficeUnit']['Office']['id']);
            $this->Session->write('User.office_name', $user['OfficeUnit']['Office']['name']);
            $this->Session->write('User.office_unit_id', $user['OfficeUnit']['id']);
            $this->Session->write('User.office_unit_name', $user['OfficeUnit']['name']);
            $this->Session->write('User.unit_type', $user['OfficeUnitUser']['type']);
            $this->Session->write('User.office_unit_user_id', $user['OfficeUnitUser']['id']);
            $this->Session->write('User.treatment_duration', $user['OfficeUnitUser']['treatment_duration']);
        }
    }
}

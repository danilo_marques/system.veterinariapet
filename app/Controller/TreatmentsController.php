<?php

class TreatmentsController extends AppController {

	public $uses = array('Treatment', 'TreatmentTemplate', 'TreatmentField', 'TreatmentValue', 'Pet', 'Customer');
	public $helpers = array('Date');

	public function restrict_index($id) {

		$pet = $this->Pet->find('first', array(
				'fields' => array('Pet.id', 'Pet.name', 'Pet.birth', 'Pet.avatar'),
				'conditions' => array('Pet.id' => $id),
				'recursive' => 0
			)
		);

		$treatment_templates = $this->TreatmentTemplate->find('all', array(
				'fields' => array('TreatmentTemplate.id', 'TreatmentTemplate.name', 'TreatmentTemplate.default'),
				'conditions' => array(
					'TreatmentTemplate.office_unit_user_id' => $id,
					'TreatmentTemplate.status' => 1,
				),
				'recursive' => 0
			)
		);

		$treatments = $this->Treatment->find('all', array(
				'contain' => array(
					'TreatmentValue', 
					'TreatmentTemplate' => array(
						'TreatmentField' => array(
							'order' => array('TreatmentField.field_order' => 'asc')
						)
					),
					'OfficeUnitUser' => array(
						'User.name'
					)
				),
				'conditions' => array(
					'Treatment.pet_id' => $id,
					'Treatment.office_unit_user_id' => $this->Session->read('User.office_unit_user_id')
				),
				'order' => array('Treatment.created' => 'asc')
			)
		);

		$this->set(compact('pet', 'treatment_templates', 'treatment_fields', 'treatments'));
	}

	public function restrict_add() {
		if($this->data) {
			if($this->TreatmentValue->saveAll($this->data['TreatmentValue'])) {
				if($this->Treatment->saveAll($this->data['Treatment'])) {
					$this->Session->setFlash('O atendimento foi salvo com sucesso!', 'success');
				}
			} else {
				$this->Session->setFlash('Ocorreu um erro ao salvar o atendimento!', 'error');
			}

			$this->redirect('/atendimento/'.$this->data['Treatment']['pet_id']);
		}
	}

	public function restrict_view_templates() {
		$templates = $this->TreatmentTemplate->find('all', array(
				'conditions' => array('TreatmentTemplate.status' => 1),
				'recursive' => -1
			)
		);

		$this->set(compact('templates'));
	}

	public function restrict_edit_templates($id) {
		$treatment_template = $this->TreatmentTemplate->find('first', array(
				'fields' => array('TreatmentTemplate.id', 'TreatmentTemplate.name'),
				'conditions' => array(
					'TreatmentTemplate.id' => $id,
					'TreatmentTemplate.status' => 1,
				),
				'recursive' => 0
			)
		);

		$treatment_fields = $this->TreatmentField->find('all', array(
				'conditions' => array(
					'TreatmentField.treatment_template_id' => $treatment_template['TreatmentTemplate']['id']
				),
				'order' => array('TreatmentField.field_order' => 'asc'),
				'recursive' => -1
			)
		);

		$this->set(compact('treatment_template', 'treatment_fields'));
	}
}
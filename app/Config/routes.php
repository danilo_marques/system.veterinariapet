<?php
/**
 * Routes configuration
 */
	/* Login */
	Router::connect('/login/pessoal', array('controller' => 'Users', 'action' => 'login'));
	Router::connect('/login/profissional', array('controller' => 'Users', 'action' => 'login'));

	/* Logout */
	Router::connect('/logout', array('controller' => 'Users', 'action' => 'logout'));

	/* Primeiro acesso */
	Router::connect('/primeiro-acesso/configuracoes', array('controller' => 'AgendaConfigurations', 'action' => 'first_access', 'restrict' => true));

	/* Agenda */
	Router::connect('/', array('controller' => 'Agendas', 'action' => 'index', 'restrict' => true));
	Router::connect('/agenda', array('controller' => 'Agendas', 'action' => 'index', 'restrict' => true));
	Router::connect('/agenda/adicionar', array('controller' => 'Agendas', 'action' => 'add', 'restrict' => true));
	Router::connect('/agenda/reagendar', array('controller' => 'Agendas', 'action' => 'edit', 'restrict' => true));
	
	/* Configurações da agenda */
	Router::connect('/configuracoes/agenda', array('controller' => 'AgendaConfigurations', 'action' => 'add', 'restrict' => true));
	Router::connect('/configuracoes/bloqueio', array('controller' => 'AgendaConfigurations', 'action' => 'blockade', 'restrict' => true));

	/* Usuários do sistema */
	Router::connect('/usuarios', array('controller' => 'Users', 'action' => 'index', 'restrict' => true));
	Router::connect('/usuarios/cadastrar', array('controller' => 'Users', 'action' => 'add', 'restrict' => true));
	Router::connect('/usuarios/visualizar', array('controller' => 'Users', 'action' => 'view', 'restrict' => true));
	Router::connect('/usuarios/editar', array('controller' => 'Users', 'action' => 'edit', 'restrict' => true));
	Router::connect('/usuarios/busca', array('controller' => 'Users', 'action' => 'search', 'restrict' => true));

	/* Pets */
	Router::connect('/pets/adicionar', array('controller' => 'Pets', 'action' => 'add', 'restrict' => true));
	Router::connect('/pets/visualizar/*', array('controller' => 'Pets', 'action' => 'view', 'restrict' => true));
	Router::connect('/pets/editar', array('controller' => 'Pets', 'action' => 'edit', 'restrict' => true));
	Router::connect('/pets/buscar', array('controller' => 'Pets', 'action' => 'search', 'restrict' => true));

	/* Atendimento */
	Router::connect('/atendimento/salvar', array('controller' => 'Treatments', 'action' => 'add', 'restrict' => true));
	Router::connect('/atendimento/:id', array('controller' => 'Treatments', 'action' => 'index', 'restrict' => true),
        array('pass' => array(
                'id'
            )
        )
    );

	/* Configurações do prontuário */
	Router::connect('/configuracoes/prontuarios', array('controller' => 'Treatments', 'action' => 'view_templates', 'restrict' => true));
	Router::connect('/configuracoes/prontuarios/*', array('controller' => 'Treatments', 'action' => 'edit_templates', 'restrict' => true));

	/* Clientes */
	Router::connect('/clientes/adicionar', array('controller' => 'Customers', 'action' => 'add', 'restrict' => true));

	/* Dashboard */
	Router::connect('/dashboard', array('controller' => 'Dashboards', 'action' => 'index', 'restrict' => true));

	/* Financeiro */
	Router::connect('/financeiro', array('controller' => 'Finances', 'action' => 'index', 'restrict' => true));

	/* Configurações */
	Router::connect('/configuracoes', array('controller' => 'pages', 'action' => 'display', 'restrict_configurations'));

	/* Pages */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();
	require CAKE . 'Config' . DS . 'routes.php';

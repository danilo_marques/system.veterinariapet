$(document).ready(function() {
	/* Autocomplete de clientes */
	// $('#vp-form-add-agenda-select2-customer-name').select2({
 //        placeholder: "Digite o nome do cliente",
 //        minimumInputLength: 2,
 //        width: '300px',
 //        ajax: {
 //            url: "/restrict/Customers/autocomplete",
 //            dataType: 'json',
 //            quietMillis: 100,
 //            data: function (term) {
 //                return {
 //                    name: term,
 //                };
 //            },
 //            results: function (data) {
 //                return {
 //                    results: data
 //                };
 //            }
 //        },
 //        initSelection: function (item, callback) {
 //            var data = {
 //                name: item.val()
 //            };
 //            callback(data);
 //        },
 //        formatResult: format_autocomplete_result_customer,
 //        formatSelection: format_autocomplete_selection_customer,
 //        escapeMarkup: function (m) { 
 //            return m;
 //        },
 //        formatNoMatches: function() {
 //            return '<div>Nenhum cliente encontrado com esse nome!</div>';
 //        }
 //    });
});

function initSelect2Customer() {
    $('#vp-form-add-agenda-select2-customer-name').select2({
        placeholder: "Digite o nome do cliente",
        minimumInputLength: 2,
        width: '100%',
        ajax: {
            url: "/restrict/Customers/autocomplete",
            dataType: 'json',
            quietMillis: 100,
            data: function (term) {
                return {
                    name: term,
                };
            },
            results: function (data) {
                return {
                    results: data
                };
            }
        },
        initSelection: function (item, callback) {
            var data = {
                name: item.val()
            };
            callback(data);
        },
        formatResult: format_autocomplete_result_customer,
        formatSelection: format_autocomplete_selection_customer,
        escapeMarkup: function (m) { 
            return m;
        },
        formatNoMatches: function() {
            return '<div>Nenhum cliente encontrado com esse nome!</div>';
        }
    });
}

/* Função auxiliar da select2 autocomplete - Monta a visualização */
function format_autocomplete_result_customer(data) {
    var markup = "<table class='data-result'><tr>"
    + "<td class='data-name'><div>" + data.name + "</div>"
    + "</td></tr></table>";

    return markup;
}

/* Função auxiliar da select2 autocomplete - Seleção do item */
function format_autocomplete_selection_customer(data) {
	if(data.name) {
        $.ajax({
            url: '/restrict/Pets/get_by_customer_id',
            async: false,
            type: 'post',
            dataType : 'json',
            data : {customer_id: data.id},
            success: function(data) {

                $("#vp-form-add-agenda-select2-pet-name").empty();
                $('#vp-form-add-agenda-select2-pet-name').select2('data', {id: null, text: ''});

                $.each(data, function(key, pet) {
                    $('#vp-form-add-agenda-select2-pet-name').append('<option value="'+key+'">'+pet+'</option>');
                });

                $('#vp-form-add-agenda-select2-pet-name').select2({width:'100%', minimumResultsForSearch: -1});
            }
        });

        $('#vp-form-add-agenda-customer-phone').val(data.phone);
        $('#vp-form-add-agenda-customer-cellphone').val(data.cellphone);

    	$('#vp-select2-pet-name').removeClass('hide');
    }

    return data.name;
}
/* Add here all your JS customizations */

$(document).ready(function(){
	/* Popula o lightbox de confirmação de exclusão de period da agenda com o id - Periodo específico */
	$('body').on('click', '.vp-delete-period', function() {
		$('#modalConfirmDeletationPeriod #period_id').val($(this).attr('period_id'));
		$('#modalConfirmDeletationPeriod #action').val('specific');
	});

	/* Popula o lightbox de confirmação de exclusão de period da agenda com o id - Todos os periodos */
	$('body').on('click', '.vp-delete-all-period', function() {
		$('#modalConfirmDeletationPeriod #period_id').val(null);
		$('#modalConfirmDeletationPeriod #action').val('all');
	});
});
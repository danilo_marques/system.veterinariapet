$(document).ready(function() {
	/* initialize the javascript */
	App.init();

	/* Marca no menu e header a página a qual profissional está localizado */
	var page = $('#cl-wrapper').attr('page');
	var subPage = $('#cl-wrapper').attr('subPage');
	
	$('li.menu-' + page).addClass('active');
	$('li.menu-' + page + '-' + subPage).addClass('active');


	$('body').on('click', '.ckb_select_all', function() {
		var checkbox = $(this).attr('ckb_select');

        if(this.checked) {
            $('.' + checkbox).each(function() {
                this.checked = true;
            });
        } else {
            $('.' + checkbox).each(function() {
                this.checked = false;
            });
        }
	});
});
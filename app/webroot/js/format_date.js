/* 
* Formata datas do padrão js (Sun Sep 07 2014 13:30:23 GMT-0300 (BRT)) para o padrao PT-BR 00/00/0000
*/
function formatDate(date) {
	if(!date) date = new Date();
	
	var day = date.getDate();
	var month = date.getMonth() + 1;
	var year = date.getFullYear();

	if(day<10) day = "0"+day;
	if(month<10) month = "0"+month; 

	new_date = day + '/' + month + '/' + year;

	return new_date;
}

function formatTime(time) {
	if(!time) time = new Date();

	var hours = time.getHours();
	var minutes = time.getMinutes();

	if(hours<10) hours="0"+hours; 
	if(minutes<10) minutes="0"+minutes; 

	new_time = hours + ':' + minutes;

	return new_time;
}
$(document).ready(function() {
	/* Autocomplete pets */
    // $('.vp-form-add-agenda-select2-pet-name').select2({
    //     placeholder: "Digite o nome",
    //     minimumInputLength: 2,
    //     width: '300px',
    //     ajax: {
    //         url: "/restrict/ Pets/autocomplete",
    //         dataType: 'json',
    //         quietMillis: 100,
    //         data: function (term) {
    //             return {
    //                 name: term,
    //                 customer_id: $('#vp-form-add-agenda-customer-id').val()
    //             };
    //         },
    //         results: function (data) {
    //             return {
    //                 results: data
    //             };
    //         }
    //     },
    //     initSelection: function (item, callback) {
    //         var data = {
    //             name: item.val()
    //         };
    //         callback(data);
    //     },
    //     formatResult: format_autocomplete_result_pet,
    //     formatSelection: format_autocomplete_selection_pet,
    //     escapeMarkup: function (m) { 
    //         return m;
    //     },
    //     formatNoMatches: function() {
    //         return '<div>Não foi encontrado nenhum resultado!</div>';
    //     }
    // });

    $('body').on('change', '.vp-form-search-pet-by', function() {
        if($(this).val() == 'pet') {
            $('.vp-label-search-pet').text('Nome do pet:');
            $('#vp-form-search-pet-name').attr('placeholder', 'Nome do pet');
        } else {
            $('.vp-label-search-pet').text('Nome do cliente:');
            $('#vp-form-search-pet-name').attr('placeholder', 'Nome do cliente');
        }
    });

    $('body').on('click', '.vp-box-result-search', function() {
        $(location).attr('href', '/pets/visualizar/' + $(this).attr('pet_id'));
    });
});

/* Função auxiliar da select2 autocomplete - Monta a visualização */
function format_autocomplete_result_pet(data) {
    var markup = "<table class='data-result'><tr>"
    + "<td class='data-name'><div>" + data.name + "</div>"
    + "</td></tr></table>";

    return markup;
}

/* Função auxiliar da select2 autocomplete - Seleção do item */
function format_autocomplete_selection_pet(data) {
	if(data.name) {
    	$('#vp-form-add-agenda-pet-id').val(data.id);
    }

    return data.name;
}

/* Exibe os dados de confirmação do cadastro de pets */
function form_wizard_confirmation() {
    //Data Customer
    $('#vp-confirm-customer-name').val($('#vp-form-add-pet-customer-name').val());
    $('#vp-confirm-customer-birth').val($('#vp-form-add-pet-customer-birth').val());
    $('#vp-confirm-customer-gender').val($('#vp-form-add-pet-customer-gender').val());
    $('#vp-confirm-customer-email').val($('#vp-form-add-pet-customer-email').val());
    $('#vp-confirm-customer-phone').val($('#vp-form-add-pet-customer-phone').val());
    $('#vp-confirm-customer-cellphone').val($('#vp-form-add-pet-customer-cellphone').val());
    $('#vp-confirm-customer-commercial_phone').val($('#vp-form-add-pet-customer-commercial_phone').val());

    //Data Address
    $('#vp-confirm-address-zip').val($('#vp-form-add-pet-address-zip').val());
    $('#vp-confirm-address-address').val($('#vp-form-add-pet-address-address').val());
    $('#vp-confirm-address-number').val($('#vp-form-add-pet-address-number').val());
    $('#vp-confirm-address-complement').val($('#vp-form-add-pet-address-complement').val());
    $('#vp-confirm-address-state').val($('#vp-form-add-pet-address-state').val());
    $('#vp-confirm-address-city').val($('#vp-form-add-pet-address-city').val());
    $('#vp-confirm-address-district').val($('#vp-form-add-pet-address-district').val());

    //Data Pet
    $('#vp-confirm-pet-name').val($('#vp-form-add-pet-pet-name').val());
    $('#vp-confirm-pet-pet_type').val($('#vp-form-add-pet-pet-pet_type').val());
    $('#vp-confirm-pet-gender').val($('#vp-form-add-pet-pet-gender').val());
    $('#vp-confirm-pet-birth').val($('#vp-form-add-pet-pet-birth').val());
    $('#vp-confirm-pet-race').val($('#vp-form-add-pet-pet-race').val());
    $('#vp-confirm-pet-pelage_color').val($('#vp-form-add-pet-pet-pelage_color').val());
}
scheduler.date.add=function(date,inc,mode){
	var ndate=new Date(date.valueOf());
	switch(mode){
		case "day": ndate.setDate(ndate.getDate()+inc); 
			if (inc == 1 && ndate.getDate()==date.getDate()){ //Brasil
				return this.add(ndate,2,"hour");
			}
		break;
		case "week": ndate.setDate(ndate.getDate()+7*inc); break;
		case "month": ndate.setMonth(ndate.getMonth()+inc); break;
		case "year": ndate.setYear(ndate.getFullYear()+inc); break;
		case "hour": ndate.setHours(ndate.getHours()+inc); break;
		case "minute": ndate.setMinutes(ndate.getMinutes()+inc); break;
		default:
			return scheduler.date["add_"+mode](date,inc,mode);
	}
	return ndate;
};
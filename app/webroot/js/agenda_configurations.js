agenda = function(){}
agenda.configurations = function(){}

agenda.init = function() {
	/* Tooltip */
	dhtmlXTooltip.config.className = 'dhtmlXTooltip tooltip'; 
	dhtmlXTooltip.config.timeout_to_display = 25; 
	dhtmlXTooltip.config.delta_x = 15; 
	dhtmlXTooltip.config.delta_y = -20;

	/* Scheduler */
	scheduler.config.xml_date = "%Y-%m-%d %H:%i";
	scheduler.config.details_on_dblclick = true;
	scheduler.config.details_on_create = true;
	scheduler.config.multi_day = true;
	scheduler.config.first_hour = 6;
	scheduler.config.drag_resize = false;
	scheduler.config.drag_move = false;
	scheduler.config.drag_create = false;
	scheduler.config.prevent_cache = true;
	scheduler.config.show_loading = true;
	scheduler.config.icons_select = false;
	scheduler.config.hour_size_px = (60 / agenda.treatment_duration) * 22;

	/* Faz o scroll automático da página */
	var now = new Date();
    scheduler.config.scroll_hour = now.getHours() - 1;

	/* Carrega as configurações da agenda do usuário */
	var agenda_configurations = agenda.configurations.load();

	$.each(agenda_configurations, function(key, agenda_configuration) {

    	var start_time = agenda_configuration.start_time.replace(':', '.');
    	var end_time = agenda_configuration.end_time.replace(':', '.');
    	var start_time_lunch = agenda_configuration.start_time_lunch.replace(':', '.');
    	var end_time_lunch = agenda_configuration.end_time_lunch.replace(':', '.');

    	var one_hour = (parseInt('01:00')*60);

    	var zones = [
    		(parseInt(start_time)*60),
    		(parseInt(start_time_lunch)*60) + one_hour,
    		(parseInt(end_time_lunch)*60) + one_hour,
    		(parseInt(end_time)*60) + one_hour
    	];

    	$.each(agenda_configuration.weekday.split(','), function(key, weekday) {
    		scheduler.addMarkedTimespan({
			 	days: [weekday],
			 	zones: zones,
			 	invert_zones: true,
			 	css: "blocked_time",
			});
    	});
	});

	/* Inicia a agenda */
	if($(window).width() <= 700) {
		scheduler.init('init_scheduler',new Date(),"day");
	} else {
		scheduler.init('init_scheduler',new Date(),"week");
	}

	/* Carrega os agendamentos do profissional */
	var url = "restrict/Agendas/load/" + scheduler._mode;
	scheduler.load(url, 'json');
}

/* Carrega as configurações da agenda do usuário */
agenda.configurations.load = function() {
	var configurations;

	$.ajax({
	 	url: '/restrict/AgendaConfigurations/set_config',
	  	type: 'get',
	  	async: false,
	  	dataType: 'json',
	  	success: function(data){
	   		configurations = data;
	  	}
	});

	return configurations;
}

/* Abre o lightbox de agendamento */
scheduler.showLightbox = function(event_id) {
	var event = scheduler.getEvent(event_id);

	var date = formatDate(event != undefined ? event.start_date : null);
	var start_time = formatTime(event != undefined ? event.start_date : null);

    initSelect2Customer();

	/* Abre o lightbox de agendamento */
	$('#vp-lightbox-new-scheduling').modal('show');

	/* Insere dados no lightbox */
	$('#dhtmlx_event_id').val(event_id);
	$('#vp-form-add-agenda-start_date').val(date);
	$('#vp-form-add-agenda-start_time').val(start_time);

	/* Ao fechar o lightbox chama a função que remove balão de agendamento */
    $('#vp-lightbox-new-scheduling').on('hide.bs.modal', function() {
		close_lightbox_scheduling();
	});
}

/* Seta a data no mini-calendar */
scheduler.templates.month_date_class = function(start, end, event) {
    return "month-day-link " + start;
};

scheduler.templates.event_text = function(start, end, event) {
	return '<strong>Pet:</strong> ' + event.text;
}

scheduler.templates.event_class = function(start, end, event) {

	switch(event.event_type) {
		case '0':
			var event_type = 'event_cancelled';
			break;
		case '1':
			var event_type = 'event_pending';
			break;
		case '2':
			var event_type = 'event_confirmed';
			break;
		case '3':
			var event_type = 'event_attended';
			break;
	}

	return event_type;
}

scheduler.templates.tooltip_text = function(start, end, event) {
	var formatDate = scheduler.date.date_to_str("%d/%m/%Y");
	var formatTime = scheduler.date.date_to_str("%H:%i");

	var html = "<strong>Cliente:</strong> " + event.customer_name + "<br />";
	html += "<strong>Pet:</strong> " + event.text + "<br />";
	html += "<strong>Data:</strong> " + formatDate(start) + "<br />";
	html += "<strong>Horário:</strong> " + formatTime(start) + ' - ' + formatTime(end);
    
    return html;
};

// scheduler.renderEvent = function(container, ev, width, height, header_content, body_content) {
// 	var container_width = container.style.width; // e.g. "105px"

// 	// move section
// 	var html = "<div class='dhx_event_move my_event_move' style='width: " + container_width + "'></div>";

// 	// container for event contents
// 	html+= "<div class='my_event_body'>";
// 		html += "<span class='event_date'>";
// 		// two options here: show only start date for short events or start+end for long
// 		if ((ev.end_date - ev.start_date) / 60000 > 40) { // if event is longer than 40 minutes
// 			html += scheduler.templates.event_header(ev.start_date, ev.end_date, ev);
// 			html += "</span><br/>";
// 		} else {
// 			html += scheduler.templates.event_date(ev.start_date) + "</span>";
// 		}
// 		// displaying event text
// 		html += "<span>" + scheduler.templates.event_text(ev.start_date, ev.end_date, ev) + "</span>";
// 	html += "</div>";

// 	// resize section
// 	html += "<div class='dhx_event_resize my_event_resize' style='width: " + container_width + "'></div>";

// 	container.innerHTML = html;
// 	return true; // required, true - we've created custom form; false - display default one instead
// };

scheduler.attachEvent("onDblClick", function(event_id, event_default) {
	var event = scheduler.getEvent(event_id);

	var formatDate = scheduler.date.date_to_str("%d/%m/%Y");
	var formatTime = scheduler.date.date_to_str("%H:%i");

	$('#vp-popover-show-scheduler .vp-popover-time').text('Horário: '+formatTime(event.start_date)+' - '+formatTime(event.end_date));
	$('#vp-popover-show-scheduler .vp-popover-customer-name').html('<strong>Cliente:</strong> ' + event.customer_name);
	$('#vp-popover-show-scheduler .vp-popover-pet-name').html('<strong>Pet:</strong> ' + event.text);
	$('#vp-popover-show-scheduler .vp-popover-customer-phone').html('<strong>Telefone:</strong> ' + event.phone);
	if(event.observations != null) {
		$('#vp-popover-show-scheduler .vp-popover-reservation-obs').html('<strong>Observação:</strong> ' + event.observations);
		$('#vp-popover-show-scheduler .vp-popover-reservation-obs').attr('title', event.observations);
	}

	$('#vp-btn-popover-treatment').attr('href', '/atendimento/' + event.pet_id);
	$('#vp-btn-popover-rescheduling').attr('reservation_id', event.id);

	/* Posição do popover */
	positionPopoverWidth = event_default.target.clientWidth;
	positionPopoverHeight = event_default.target.clientHeight;
	positionPopoverTop = event_default.clientY-28;
	positionPopoverLeft = event_default.clientX-70;

	$('#vp-popover-show-scheduler').modalPopover('show');

	if(event_default.clientY <= 200) {
		var bottomPosition = $('#vp-popover-show-scheduler').offset();
		$('#vp-popover-show-scheduler').removeClass('top').addClass('bottom');
		$('#vp-popover-show-scheduler').css('top', bottomPosition.top + 210);
	} else {
		$('#vp-popover-show-scheduler').removeClass('bottom').addClass('top');
	}

	$('#vp-show-popover').show();
	$('.dhtmlXTooltip').remove();
});

agenda.reload_schedulings = function() {
	var url = "restrict/Agendas/load/" + scheduler._mode;
	scheduler.load(url, 'json');
}

/* Exibe o dhtmlx mini-calendar */
agenda.show_minical = function(){
	if(scheduler.isCalendarVisible()){
		scheduler.destroyCalendar();
	} else {
		scheduler.renderCalendar({
			position:"dhx_minical_icon",
			date:scheduler._date,
			navigation:true,
			handler:function(date,calendar){
				scheduler.setCurrentView(date);
				scheduler.destroyCalendar();
			}
		});
	}
}

/* Remove balão do agendamento e reseta o formulário */
function close_lightbox_scheduling() {
	scheduler.setEvent($('#dhtmlx_event_id').val(), {});
    scheduler.updateView();

	$('#vp-form-add-agenda-select2-customer-name').select2('data', null);

	$('#vp-form-add-scheduling').each (function(){
		this.reset();
	});

	$('#vp-select2-pet-name').addClass('hide');
}

function close_popover() {
	$('#vp-popover-show-scheduler').modalPopover('hide');
	$('#vp-show-popover').hide();
}

$(window).resize(function() {
	if($(this).width() <= 700){
		scheduler.init('init_scheduler',new Date(),"day");
	} else {
		scheduler.init('init_scheduler',new Date(),"week");
	}
});

$(document).ready(function(){
	/* Inicia a agenda */
	agenda.init();

	/* Botão agendar consulta */
	$('body').on('click', '#vp-btn-new-scheduling', function() {
		scheduler.showLightbox();
	});

	/* Atualiza a agenda ao ocultar ou exibir o menu lateral */
	$('body').on('click', '#sidebar-collapse', function() {
		scheduler.updateView();
	});

	/* Fecha o popover ao clicar no "x" */
	$('body').on('click', '.vp-close-popover', function() {
		close_popover();
	});

	/* Exibe lightbox de confirmação de exclusão de consulta */
	$('body').on('click', '#vp-btn-popover-delete-scheduling', function() {
		close_popover();
		$('#vp-lightbox-delete-scheduling').modal('show');
	});

	/*  */
	$('body').on('click', '#vp-btn-popover-rescheduling', function() {
		close_popover();

		var request = $.ajax({
			url: "/restrict/Agendas/get_reservation_by_id",
			type: "get",
			dataType: "json",
			data: {id: $(this).attr('reservation_id')}
		});

		request.done(function(data) {
			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-reservation-id').val(data.event.Reservation.id);
			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-customer-name').val(data.event.Customer.name);
			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-pet-name').val(data.event.Pet.name);
			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-customer-phone').val(data.event.Customer.phone);
			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-customer-cellphone').val(data.event.Customer.cellphone);
			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-observation').val(data.event.Reservation.observations);

			var date_time = data.event.Reservation.start_date.split(' ');
			var current_date = date_time[0].split('-');

			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-start-date').val(current_date[2]+'/'+current_date[1]+'/'+current_date[0]);
			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-start-time').val(date_time[1]);

		    var start_time = strtotime(data.event.Reservation.start_date);
		    var end_time = strtotime(data.event.Reservation.end_date);

		    var current_time = (end_time - start_time) / 60;

			$('#vp-lightbox-rescheduling #vp-form-edit-agenda-treatment-duration').val(current_time);

			$('#vp-lightbox-rescheduling').modal('show');
		});
	});

	// $('body').on('click', '.pop_over', function() {
	// 	var position = $(this).offset();
	// 	var $this = $(this).parent().parent();

	// 	var event = scheduler.getEvent($this.attr('event_id'));

	// 	var formatDate = scheduler.date.date_to_str("%d/%m/%Y");
	// 	var formatTime = scheduler.date.date_to_str("%H:%i");

	// 	if(event.observations != null) {
	// 		var observation = event.observations;
	// 	} else {
	// 		var observation = '';
	// 	}

	// 	$('#vp-popover-show-scheduler .vp-popover-time').text('Horário: '+formatTime(event.start_date)+' - '+formatTime(event.end_date));
	// 	$('#vp-popover-show-scheduler .vp-popover-customer-name').html('<strong>Cliente:</strong> ' + event.customer_name);
	// 	$('#vp-popover-show-scheduler .vp-popover-pet-name').html('<strong>Pet:</strong> ' + event.text);
	// 	$('#vp-popover-show-scheduler .vp-popover-customer-phone').html('<strong>Telefone:</strong> ' + event.phone);
	// 	$('#vp-popover-show-scheduler .vp-popover-reservation-obs').html('<strong>Observação:</strong> ' + observation);
	// 	$('#vp-popover-show-scheduler .vp-popover-reservation-obs').attr('title', observation);

	// 	positionPopoverWidth = $this.width();
	// 	positionPopoverHeight = $this.height();
	// 	positionPopoverTop = position.top-20;
	// 	positionPopoverLeft = position.left;

	// 	$('#vp-popover-show-scheduler').modalPopover('show');
	// 	$('#vp-show-popover').show();
	// 	$('.dhtmlXTooltip').remove();
	// });

	// $('body').on('click', '.dhx_cal_event', function() {

	// 	var position = $(this).offset();

	// 	positionPopoverTop = position.top - 100;
	// 	positionPopoverLetf = position.left - 130;

	// 	$('#popupTop').modalPopover('show');
	// });

	// scheduler.addMarkedTimespan({
	// 	days: new Date(2014,6,26),
	// 	zones: [9*60,13*60,14*60,19*60],
	// 	invert_zones: true,
	// 	css: "red_section",
	// });

	// scheduler.updateView();

	// scheduler.config.xml_date="%Y-%m-%d %H:%i";
	// scheduler.config.multi_day = true;
	// scheduler.config.first_hour = 5;
	// scheduler.config.drag_resize = false;
	// scheduler.config.drag_move = false;
	// scheduler.config.drag_create = false;

	/* Um dia especifico inteiro */
		// scheduler.addMarkedTimespan({
		// 	days:  new Date(2012,7,6),
		// 	zones: 'fullday',
		// 	css: "blue_section",
		// });
	/* Um dia especifico inteiro - End */
	

	/* Mais de um dia específico inteiro */
		// scheduler.addMarkedTimespan({
		// 	start_date: new Date(2012,7,12), 
		// 	end_date: new Date(2012,7,14), 
		// 	css: "green_section",
		// });
	/* Mais de um dia específico inteiro - End */


	/* Vários dias em horários específicos */
		// var dates = [];

		// dates[0] = new Date(2012,7,6);
		// dates[1] = new Date(2012,7,8);
		// dates[2] = new Date(2012,7,10);

		// for(x = 0;x < 3; x++) {
		// 	scheduler.addMarkedTimespan({
		// 		days:  dates[x],
		// 		zones: [4*60,8*60,12*60,15*60],
		// 		css: "blue_section"
		// 	});
		// }
	/* Vários dias em horários específicos - End */


	/* Vários dias o dia inteiro */
		// var dates = [];

		// dates[0] = new Date(2012,7,6);
		// dates[1] = new Date(2012,7,8);
		// dates[2] = new Date(2012,7,10);

		// for(x = 0;x < 3; x++) {
		// 	scheduler.addMarkedTimespan({
		// 		days:  dates[x],
		// 		zones: 'fullday',
		// 		css: "blue_section",
		// 	});
		// }
	/* Vários dias o dia inteiro - End */
	
	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,6), end_date: new Date(2012,7,7), css: "red_section" });
	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,7), end_date: new Date(2012,7,8), css: "fat_lines_section" });
	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,11), end_date: new Date(2012,7,12), css: "yellow_section" });
	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,12), end_date: new Date(2012,7,13), css: "dots_section" });

	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,13), end_date: new Date(2012,7,14), css: "green_section" });
	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,14), end_date: new Date(2012,7,15), css: "medium_lines_section" });
	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,18), end_date: new Date(2012,7,19), css: "blue_section" });
	// scheduler.addMarkedTimespan({start_date: new Date(2012,7,19), end_date: new Date(2012,7,20), css: "small_lines_section" });

	// scheduler.init('scheduler_here',new Date(),"day");
});
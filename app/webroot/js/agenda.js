$(document).ready(function() {

	/* Salva um novo agendamento */
	$('body').on('click', '#btn-save-scheduling', function() {

		$.ajax({
			url: '/agenda/adicionar',
			data: $('form#vp-form-add-scheduling').serialize(),
			type: 'post',
			dataType: 'json',
			success: function(data) {
				if(data.status == 'success') {
					scheduling_lightbox_success('scheduling');
				} else {
					/* Reopen lightbox */
				}
			},
			error: function(e) {
				/* Reopen lightbox */
			}
		});
	});

	/* Salva um novo agendamento */
	$('body').on('click', '#btn-rescheduling', function() {

		$.ajax({
			url: '/agenda/reagendar',
			data: $('form#vp-form-rescheduling').serialize(),
			type: 'post',
			dataType: 'json',
			success: function(data) {
				if(data.status == 'success') {
					scheduling_lightbox_success('rescheduling');
				} else {
					/* Reopen lightbox */
				}
			},
			error: function(e) {
				/* Reopen lightbox */
			}
		});
	});

	$('body').on('click', '#vp-btn-confirm-delete-scheduling', function() {
		console.log('Apaga');
	});

	/* Exibe o lightbox de exclusão de todos os horários de atendimento da agenda */
	$('body').on('click', '.vp-delete-all-period', function() {
		$('#vp-text-confirmation').html('Deseja realmente apagar todas as configurações de horários?');
		
		$('#vp-lightbox-confirm-delete #agenda-configuration-id').val(null);
		$('#vp-lightbox-confirm-delete #action').val('delete-all');

		$('#vp-lightbox-confirm-delete').modal('show');
	});

	/* Exibe o lightbox de exclusão de um horário de atendimento específico da agenda */
	$('body').on('click', '.vp-delete-period', function() {
		$('#vp-text-confirmation').html('Deseja realmente apagar essa configuração de horário?');

		$('#vp-lightbox-confirm-delete #agenda-configuration-id').val($(this).attr('agenda_configuration_id'));
		$('#vp-lightbox-confirm-delete #action').val('delete-this');

		$('#vp-lightbox-confirm-delete').modal('show');
	});

	/* Ajax de exclusão de períodos de atendimento da agenda */
	$('body').on('click', '#vp-btn-delete-config-agenda', function() {
		var agenda_config_id = $('#vp-lightbox-confirm-delete #agenda-configuration-id').val();
		var action = $('#vp-lightbox-confirm-delete #action').val();

		if(action == 'delete-all') {
			var request = $.ajax({
				url: "/restrict/AgendaConfigurations/delete",
				type: "post",
				dataType: "json",
				data: {action: 'delete-all'}
			});

			request.done(function(data) {
				$(location).attr('href', '/configuracoes/agenda');
			});
		} else {
			var request = $.ajax({
				url: "/restrict/AgendaConfigurations/delete",
				type: "post",
				dataType: "json",
				data: {action: 'delete-this', agenda_configuration_id: agenda_config_id}
			});

			request.done(function(data) {
				$(location).attr('href', '/configuracoes/agenda');
			});
		}
	});
});

/* Função de sucesso do lightbox de agendamento */
function scheduling_lightbox_success(type) {

    $('#vp-lightbox-new-scheduling').modal('hide');
    $('#vp-lightbox-rescheduling').modal('hide');

    setTimeout(function(){
    	if(type == 'scheduling') {
    		$('#vp-lightbox-scheduling-successful').modal('show');
    	} else {
    		$('#vp-lightbox-rescheduling-successful').modal('show');
    	}
	}, 600);

    setTimeout(function(){
	 	$('#vp-lightbox-scheduling-successful').modal('hide');
	 	$('#vp-lightbox-rescheduling-successful').modal('hide');
	}, 6000); // 6 Segundos

    /* Reseta o form de agendamento */
	$('#vp-form-add-agenda-select2-customer-name').select2('data', null);
	$('#vp-select2-pet-name').addClass('hide');

	$('form#vp-form-add-scheduling').each (function(){
		this.reset();
	});

	/* Recarrega os agendamentos da agenda */
	agenda.reload_schedulings();
}